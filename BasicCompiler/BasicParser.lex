%{
#include<stdio.h>
#include<stdlib.h>
#include "yacc_parser.tab.hh"
#include <iostream>
using namespace std;
%}
digit [0-9]
alpha [a-zA-Z]
ascii [\x21-\x7E\ ]
%option noyywrap
%%
"class" {return CLASS;}
"main" {return MAIN;}
"int" {return type_int;}
"bool" {return type_bool;}
"char" {return type_char;}
"<=" {return le_relop;}
">=" {return ge_relop;}
"==" {return eq_relop;}
"!=" {return ne_relop;}
"&&" {return and_condop;}
"||" {return or_condop;}
"true" {return bool_li;}
"false" {return bool_li;}
"print" {return print;}
"read" {return READ;}
"if" {return if_cond;}
"then" { return THEN;}
"goto" {return goto_cond;}
[=] {return equals;}
[+] {return add_op;}
[-] {return sub_op;}
[*] {return mul_op;}
[/] {return div_op;}
[<] {return lt_relop;}
[>] {return gt_relop;}
[!] {return not_condop;}
[(] {return open_brcs;} 
[)] {return close_brcs;} 
[{] {return open_fbrcs;} 
[}] {return close_fbrcs;} 
[[] {return open_sbrcs;} 
[]] {return close_sbrcs;} 
[;] {return semi_col;}
[,] {return comma;}
[:] {return col;}
{digit}({digit})* {return integer;}
{alpha}({alpha}|"_"|(digit)*)* { return id;}
\"(\\.|[^"])*\" {return STRING;}
\'{alpha}\' {return character;}
[ \t\n]+ { }
<<EOF>>  { return end; }
.        { cerr << "Unrecognized token!" << endl; exit(1); }
%%
