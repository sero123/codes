%{
#include <iostream>
#include <string>
#include <map>
#include <stdio.h>
#include <cstdlib> //-- I need this for atoi
using namespace std;
extern "C" FILE * yyin;
//-- Lexer prototype required by bison, aka getNextToken()
int yylex(); 
int yyerror(const char *p) { cerr << "Error!" << endl; }
%}
%token  end CLASS MAIN integer id equals STRING character type_int type_bool type_char add_op sub_op mul_op div_op lt_relop gt_relop le_relop ge_relop eq_relop ne_relop and_condop or_condop not_condop open_brcs close_brcs open_fbrcs close_fbrcs open_sbrcs close_sbrcs semi_col col comma bool_li print READ if_cond goto_cond THEN 
%left ge_relop gt_relop le_relop lt_relop and_condop or_condop not_condop eq_relop ne_relop 
%left mul_op div_op add_op sub_op 
%right equals
%%
program : CLASS MAIN open_fbrcs Field_Decls Statements close_fbrcs end  {{printf("Successful !! Code has no Syntax errors\n");exit(0);}} 
           | CLASS MAIN open_fbrcs Field_Decls close_fbrcs end{{printf("Successful !! Code has no Syntax errors\n");exit(0);}} 
           | CLASS MAIN open_fbrcs Statements close_fbrcs  end {{printf("Successful !! Code has no Syntax errors\n");exit(0);}} 
           | CLASS MAIN open_fbrcs close_fbrcs end  {{printf("Successful !! Code has no Syntax errors\n");exit(0);} }
Field_Decls : Field_Decl Field_Decls
            | Field_Decl 
Statements : Statement Statements | Statement
Field_Decl : Type Decls semi_col
Decls : Decl 
      |Decl comma Decls
Decl : id 
     | id open_sbrcs integer close_sbrcs 
Type : type_int 
      |type_char
      |type_bool
Statement : Labelled_Statement
          | Location equals Expr semi_col
          | if_cond Expr THEN goto_cond label semi_col
          | goto_cond label semi_col
          | Method_Call semi_col
Labelled_Statement : label Statement
Location : id
          |id open_sbrcs Expr close_sbrcs
Expr : Literal | Location|not_condop Expr|open_brcs Expr close_brcs
      |Expr lt_relop Expr
      |Expr gt_relop Expr
      |Expr le_relop  Expr
      |Expr ge_relop Expr
      |Expr eq_relop Expr
      |Expr ne_relop Expr
      |Expr and_condop Expr
      |Expr or_condop Expr
      |Expr add_op Expr
      |Expr sub_op Expr
      |Expr mul_op Expr
      |Expr div_op Expr
Method_Call : print open_brcs Exprs close_brcs 
              |READ open_brcs Location close_brcs
Exprs : Expr comma Exprs | Expr
label : id col
Literal : integer | STRING | character | bool_li

%%
//-- FUNCTION DEFINITIONS ---------------------------------
int main()
{
 char filename[100];
  scanf("%s",filename);
  // open a file handle to a particular file:
 FILE *myfile = fopen(filename, "r"); 
// make sure it is valid: 
if (!myfile) { cout << "I can't open the file!" << endl; return -1; } 
// set flex to read from it instead of defaulting to STDIN:
 yyin = myfile; // parse through the input until there is no more: 
 while (!feof(yyin))
 {
yyparse();}
}
