x = [0 1 -2 3 4 -2 -1 4 -7 4 2 -1 -5] 
h = [0 0 0 0 1 0 0 0 0];
m = length(x);
n = length(h);
k = max(m,n);
h=[h zeros(1,k-length(h))];
%k=300;
my_result = zeros(1,30);
for i = -k:k
for j = 1:k
if j-i+1 <= k & j-i+1 > 0
my_result(i+k+1) = my_result(i+k+1)+(x(j) * h(j-i+1));
end
end
end
subplot(2,2,1)
plot(x);
subplot(2,2,2);
plot(h);
subplot(2,2,[3,4]);
plot(my_result);
%y=xcorr(x,h);
%subplot(2,2,4);
%plot(y);
