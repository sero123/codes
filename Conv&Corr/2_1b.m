x = [0 1 -2 3 4 -2 -1 4 -7 4 2 -1 -5] 
h = [0.2 0.2 0.2 0.2 0.2];
m = length(x);
n = length(h);
k = max(m,n);
h=[h zeros(1,k-length(h))];
%k=9;

my_result = zeros(1,21);
for i = 1:m+n-1
for j = max(1,i+1-n):min(i,m)
if (i-j+1) < n & i-j+1 > 0
my_result(i) = my_result(i)+(x(j) * h(i-j+1));
end
end
end
subplot(2,2,1)
plot(x);
subplot(2,2,2);
plot(h);
%subplot(2,2,3);
%plot(my_result);
y=conv(x,h);
subplot(2,2,[3,4]);
plot(y);