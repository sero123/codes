#lang racket

;;; A unit substitution is a singleton substitution with a
;;; domain of just one id.  
(require eopl/eopl)

(require "ast.rkt")
(require "fb.rkt")
(require "ct.rkt")
(require "parser.rkt")

(provide
  unit-subst
  unit-subst?
  make-unit-subst
  unit-subst-id
  unit-subst-ast
  fresh?
  apply-subst
  alpha-convert
  alpha-equal?
  )
 

(define-datatype unit-subst unit-subst?
  [make-unit-subst (id id?) (ast ast?)])

;;; unit-subst-id : unit-subst? -> id?
(define unit-subst-id
  (lambda (s)
    (cases unit-subst s
      [make-unit-subst (ids asts) ids])))


;;; unit-subst-id : unit-subst? -> ast?
(define unit-subst-ast 
  (lambda (s)
    (cases unit-subst s
      [make-unit-subst (ids asts) asts])))


;;; [id? unit-subst?] -> boolean?
(define fresh? 
  (lambda (x s)
    (cases unit-subst s
      [make-unit-subst (ids asts) (and 
                                   (not (eq? x ids))
                                   (not (free? x asts)))])))
                                       
                          


(require rackunit)

(check-equal? (fresh? 'x (make-unit-subst 'z (id-ref 'w))) #t)
(check-equal? (fresh? 'x (make-unit-subst 'z (id-ref 'x))) #f)
(check-equal? (fresh? 'x (make-unit-subst 'z (id-ref 'x))) #f)
(check-equal? (fresh? 'x (make-unit-subst 'x (id-ref 'y))) #f)
(define rlist 
  (list 'ren1 'ren2 'ren3 'ren4 'ren5 'ren6 'ren7 'ren8 'ren9 'ren10 'ren11 'ren12 'ren13 'ren14 'ren15 'ren16 'ren17 'ren18 'ren19 'ren20))

(define flist
  (lambda (l)
    (let ([x (first l)])
      (begin (remove (first l) rlist ) x))))

;;; [unit-subst?  ast?] -> ast?
(define apply-subst 
  (lambda (subs as)
    (cases ast as
      [id-ref (x) (if (eq? x (unit-subst-id subs)) (unit-subst-ast subs) (id-ref x))]
      [app (rator rand) (app (apply-subst subs rator) (apply-subst subs rand))]
      [function (formal body) (if (fresh? formal subs) (function formal (apply-subst subs body)) 
                                  (apply-subst subs (alpha-convert (flist rlist) formal body)))
                                    ])))



 
;;; alpha-convert : [id? id? ast?] -> function-ast?
(define alpha-convert
  (lambda (z formal body)
    (let ([new-subst (make-unit-subst formal (id-ref z))])
      (function z (apply-subst new-subst body)))))



;;; alpha-equal? return true if its two argument ast's are
;;; alpha-equivalent, false otherwise.
;;; alpha-equal? : [ast? ast?] -> boolean?



(define alpha-equal? 
  (lambda (a b)
    (cases ast a
      [id-ref (i1) (cases ast b
                     [id-ref (i2) (eq? i1 i2)]
                     [app (ra rd) #f]
                     [function (f body) #f]
                     )]
      [app (ra1 rd1) (cases ast b
                     [id-ref (i2) #f]
                     [app (ra rd) (and (alpha-equal? ra1 ra) (alpha-equal? rd1 rd))]
                     [function (f body) #f]
                     )]
      [function (f1 body1) (cases ast b
                     [id-ref (i2) #f]
                     [app (ra rd) #f]
                     [function (f body) (if (eq? f f1) (alpha-equal? body1 body) 
                                            (alpha-equal? body1 (apply-subst (make-unit-subst f (id-ref f1)) body) ))]
                     )])))





;;; unit tests
;;; ===========

;;; s1 = [x:z]
(define s1 (make-unit-subst 'x (id-ref 'z)))

(check-equal? (apply-subst s1 (id-ref 'y)) (id-ref 'y) "apply-subst test 01")
(check-equal? (apply-subst s1 (id-ref 'x)) (id-ref 'z) "apply-subst test 02")

(check-equal?
  (apply-subst
    s1
    (app
      (id-ref 'x)
      (id-ref 'y)))
  (app (id-ref 'z)
    (id-ref 'y))
   "apply-subst test 03")

(check-equal?
  (apply-subst s1 (function 'y (id-ref 'y)))
  (function 'y (id-ref 'y))
    "apply-subst test 04")

;;; substitution  application involving renaming.
;;; ==============================================

;;; Naive rule for pushing substitution inside a lambda:
;;; (lambda (x) e)[s]
;;; = (lambda (x) e[s])

;;; 

;;; s1 = [x:z]

(define parse-ct
  (lambda (exp)
    (parse (ct exp))))

;;; Freshness condition 1 violated because formal is in the
;;; domain of s1.
;;; (lambda (x) x)[x:z]  ;; formal is in dom(s)
;;; Naive substitution results in
;;; (lambda (x) z)       ;; incorrect!
;;; Solution:
;;; Rename bound variable to w. 
;;; Apply-subst automatically does
;;; on-the-fly renaming. 

(check-true
 (alpha-equal?
   (apply-subst  s1 (parse-ct '(lambda (x) x)))
   (parse-ct '(lambda (w) w)))
 "apply-subst 09")

;;; Freshness condition 2  violated because formal 
;;; is  free in the  cod of s1.
;;; (lambda (z) x)[x:z]  ;; formal is free in cod(s)
;;; Naively applying the substitution rule results in
;;; (lambda (z) z)     ;; z in now inadvertently captured. 
;;; Solution:
;;; Rename to (lambda (w) x)[x:z]
;;;
;;; Apply-subst does
;;; on-the-fly renaming to restore  freshness.

(check-true
 (alpha-equal?
   (apply-subst  s1 (parse (ct '(lambda (z) x))))
   (parse (ct '(lambda (w) z))))
 "apply-subst? 10")

      

(check-false
  (alpha-equal?
    (id-ref 'x)
    (id-ref 'y))
  "alpha-equal? 01")

(check-true
  (alpha-equal?
    (id-ref 'x)
    (id-ref 'x))
  "alpha-equal? 02")

(check-false
  (alpha-equal?
    (app (id-ref 'x) (id-ref 'y))
    (app (id-ref 'x) (id-ref 'x)))
  "alpha-equal? 03")

(check-true
  (alpha-equal?
    (app (id-ref 'x) (id-ref 'x))
    (app (id-ref 'x) (id-ref 'x)))
  "alpha-equal? 04")


(check-true
  (alpha-equal?
    (function 'x (id-ref 'x))
    (function 'y (id-ref 'y)))
  "alpha-equal? 05")

(check-false
  (alpha-equal?
    (function 'x (id-ref 'y))
    (function 'y (id-ref 'y)))
  "alpha-equal? 06")

(check-false
  (alpha-equal?
    (function 'x (id-ref 'y))
    (function 'y (id-ref 'x)))
  "alpha-equal? 07")

(check-true
  (alpha-equal?
    (function 'x (id-ref 'z))
    (function 'y (id-ref 'z)))
  "alpha-equal? 08")







                

              
                

        

  
