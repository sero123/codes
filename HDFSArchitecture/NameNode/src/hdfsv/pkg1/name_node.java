/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hdfsv.pkg1;

import com.google.protobuf.GeneratedMessage.Builder;
import com.google.protobuf.InvalidProtocolBufferException;
import hdfsv.pkg1.HDFS.AssignBlockRequest;
import hdfsv.pkg1.HDFS.AssignBlockResponse;
import hdfsv.pkg1.HDFS.BlockLocationRequest;
import hdfsv.pkg1.HDFS.BlockLocationResponse;
import hdfsv.pkg1.HDFS.BlockLocations;
import hdfsv.pkg1.HDFS.BlockReportRequest;
import hdfsv.pkg1.HDFS.BlockReportResponse;
import hdfsv.pkg1.HDFS.CloseFileRequest;
import hdfsv.pkg1.HDFS.CloseFileResponse;
import hdfsv.pkg1.HDFS.DataNodeLocation;
import hdfsv.pkg1.HDFS.HeartBeatRequest;
import hdfsv.pkg1.HDFS.HeartBeatResponse;
import hdfsv.pkg1.HDFS.ListFilesRequest;
import hdfsv.pkg1.HDFS.ListFilesResponse;
import hdfsv.pkg1.HDFS.OpenFileRequest;
import hdfsv.pkg1.HDFS.OpenFileResponse;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javafx.util.Pair;

/**
 *
 * @author unique
 */
public class name_node implements INameNode {

    public name_node() {
        DataNodeLocation.Builder DN0 = DataNodeLocation.newBuilder();
        DN0.setIp(1000);
        DN0.setPort(1099);
        DataNodeLocation.Builder DN1 = DataNodeLocation.newBuilder();
        DN1.setIp(1001);
        DN1.setPort(1099);
        DataNodeLocation.Builder DN2 = DataNodeLocation.newBuilder();
        DN0.setIp(1002);
        DN0.setPort(1099);

    }

    private final AtomicInteger handler = new AtomicInteger();
    private final AtomicInteger dnode = new AtomicInteger();
    private String[] filehandler = new String[3000];
    private final AtomicInteger block = new AtomicInteger();
    List list = new ArrayList();
    private Map<String, Integer> fileinfo = new HashMap<>();
    private Map<Integer, Integer> blockinfo = new HashMap<>();
    private String directory = "C:/Users/Sravya/Documents/4-2/DS/Pro/HDFS/NN/";
    private String path = "C:/Users/Sravya/Documents/4-2/DS/Pro/HDFS/";

    @Override
    public byte[] openFile(byte[] in) {
        byte[] out = null;
        try {
            handler.incrementAndGet();
            OpenFileRequest ofreq = OpenFileRequest.parseFrom(in);
            String file = ofreq.getFileName();
            boolean bool = ofreq.getForRead();
            OpenFileResponse.Builder ofres = OpenFileResponse.newBuilder();
            if (bool) {
                boolean check = new File(directory, file).exists();
                if (!check) {
                    ofres.setStatus(0);

                } else {
                    ofres.setStatus(1);
                    file = directory + file;
                    String data = readFile(file);
                    System.out.println(data);
                    String[] s = data.split("\n");
                    int i = 0;
                    while (i < s.length) {
                        String k = s[i].split("-")[1];
                        ofres.addBlockNums(Integer.parseInt(k));
                        i = i + 1;

                    }

                }

            } else {
                boolean check = new File(directory, file).exists();
                if (!check) {
                    System.out.println("Ss");
                    String siz = file.substring(file.indexOf("[") + 1);
                    siz = siz.substring(0, siz.indexOf("]"));
                    System.out.println(siz + "!!");
                    file = file.substring(siz.length() + 2, file.length());
                    fileinfo.put(file, 0);
                    filehandler[handler.get()] = file;
                    ofres.setHandle(handler.get());
                    ofres.setStatus(1);
                    handler.getAndIncrement();

                } else {
                    String siz = file.substring(file.indexOf("[") + 1);
                    siz = siz.substring(0, siz.indexOf("]"));
                    file = file.substring(siz.length() + 2, file.length());
                    System.out.println(siz + "siz");
                    String data = readFile(file);
                    System.out.println(file);
                    System.out.println(data);
                    String[] s = data.split("\n");
                    int i = 0;
                    String k = "";
                    while (i < s.length) {
                        k = s[i].split("-")[2];
                        i = i + 1;

                    }
                    if (Integer.parseInt(k) <= Integer.parseInt(siz)) {

                        ofres.setHandle(handler.get());
                        ofres.setStatus(1);
                        handler.getAndIncrement();

                    } else {
                        ofres.setHandle(-1 * handler.get());
                        ofres.setStatus(1);
                        handler.getAndIncrement();

                    }

                }

            }

            out = (ofres.build().toByteArray());
        } catch (InvalidProtocolBufferException ex) {
            Logger.getLogger(name_node.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(name_node.class.getName()).log(Level.SEVERE, null, ex);
        }
        return out;
    }

    @Override
    public byte[] closeFile(byte[] in) {
        byte[] out = null;
        try {
            CloseFileResponse.Builder cfres = CloseFileResponse.newBuilder();
            CloseFileRequest cfreq = CloseFileRequest.parseFrom(in);
            int h = cfreq.getHandle();
            boolean check = new File(directory, filehandler[h]).exists();
            if (!check) {
                cfres.setStatus(0);
            } else {
                cfres.setStatus(1);

                fileinfo.put(filehandler[h], 0);
            }

            out = (cfres.build().toByteArray());

        } catch (InvalidProtocolBufferException ex) {
            Logger.getLogger(name_node.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return out;

    }

    @Override
    public byte[] getBlockLocations(byte[] in) {
        byte[] out = null;
        try {
            BlockLocationRequest blreq = BlockLocationRequest.parseFrom(in);
            int k = blreq.getBlockNums(0);
            BlockLocationResponse.Builder blres = BlockLocationResponse.newBuilder();
            blres.setStatus(k);
            BlockLocations.Builder bl = BlockLocations.newBuilder();
            bl.setBlockNumber(12);
            blres.addBlockLocations(bl);

            out = (blres.build().toByteArray());

        } catch (InvalidProtocolBufferException ex) {
            Logger.getLogger(name_node.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return out;

    }

    @Override
    public byte[] assignBlock(byte[] in) {
        byte[] out = null;
        try {
            AssignBlockRequest abreq = AssignBlockRequest.parseFrom(in);
            int hand = abreq.getHandle();
            System.out.println(hand + "hand");
            AssignBlockResponse.Builder abres = AssignBlockResponse.newBuilder();
            BlockLocations.Builder bl = BlockLocations.newBuilder();
            DataNodeLocation.Builder dl = DataNodeLocation.newBuilder();

            if (fileinfo.get(filehandler[hand]) == 1) {
                abres.setStatus(0);
            } else if (hand > 0) {
                abres.setStatus(1);
                fileinfo.put(filehandler[hand], 0);
                dnode.getAndIncrement();
                String fil = directory + Integer.toString(dnode.get() % 3) + ".txt";
                block.getAndIncrement();
                bl.setBlockNumber(dnode.get());
                blockinfo.put(block.get(), dnode.get());
                FileWriter fw = new FileWriter(fil, true);
                fw.write(Integer.toString(block.get()) + "\n");//appends the string to the file        
                fw.close();
                String fi = directory + filehandler[hand];
                fw = new FileWriter(fi, true);
                fw.write(Integer.toString(block.get()) + "-0\n");
                fw.close();

            } else {
                abres.setStatus(1);
                String file = filehandler[hand];
                String data = readFile(file);
                System.out.println(data);
                String[] s = data.split("\n");
                int i = 0;
                String k = "";
                while (i < s.length) {
                    k = s[i].split("-")[2];
                    i = i + 1;
                }
                bl.setBlockNumber(hand);

            }

            dl.setIp(122);
            bl.addLocations(dl);
            abres.setNewBlock(bl);
            out = (abres.build().toByteArray());

        } catch (IOException ex) {
            Logger.getLogger(name_node.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return out;
    }

    @Override
    public byte[] list(byte[] in) {
        byte[] out = null;
        try {
            ListFilesRequest lfreq = ListFilesRequest.parseFrom(in);
            String dir = lfreq.getDirName();
            String files;
            path = path + dir;
            File folder = new File(path);
            File[] listOfFiles = folder.listFiles();
            ListFilesResponse.Builder lfres = ListFilesResponse.newBuilder();
            for (int i = 0; i < listOfFiles.length; i++) {

                if (listOfFiles[i].isFile()) {

                    lfres.addFileNames(listOfFiles[i].getName());
                }
            }

            lfres.setStatus(1);

            out = (lfres.build().toByteArray());

        } catch (InvalidProtocolBufferException ex) {
            Logger.getLogger(name_node.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return out;

    }

    @Override
    public byte[] blockReport(byte[] in) {
        byte[] out = null;
        try {
            BlockReportRequest brreq = BlockReportRequest.parseFrom(in);
            int k = brreq.getId();
            int bn = brreq.getBlockNumbers(0);
            DataNodeLocation dnl = brreq.getLocation();
                
            BlockReportResponse.Builder brres = BlockReportResponse.newBuilder();

            brres.addStatus(1);
            out = (brres.build().toByteArray());

        } catch (InvalidProtocolBufferException ex) {
            Logger.getLogger(name_node.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return out;

    }

    @Override
    public byte[] heartBeat(byte[] in) {

        byte[] out = null;
        try {
            HeartBeatRequest hbreq = HeartBeatRequest.parseFrom(in);
            int k = hbreq.getId();
            HeartBeatResponse.Builder hbres = HeartBeatResponse.newBuilder();

            hbres.setStatus(1);
            out = (hbres.build().toByteArray());

        } catch (InvalidProtocolBufferException ex) {
            Logger.getLogger(name_node.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return out;

    }

    public String readFile(String filename) {
        String content = null;
        File file = new File(filename); //for ex foo.txt
        try {
            FileReader reader = new FileReader(file);
            char[] chars = new char[(int) file.length()];
            reader.read(chars);
            content = new String(chars);
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }
    public static void main(String args[]) {
        try {
            Registry reg = LocateRegistry.createRegistry(1096);
            reg.rebind("namenode",new name_node());
            System.out.println("Server Started");
            
        }
        catch (Exception e){
            System.out.println(e+"EEEEEEE");
        }
        
    }
}
