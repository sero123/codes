/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hdfsv.pkg1;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import hdfsv.pkg1.HDFS.BlockLocations;
import hdfsv.pkg1.HDFS.BlockReportRequest;
import hdfsv.pkg1.HDFS.DataNodeLocation;
import hdfsv.pkg1.HDFS.HeartBeatRequest;
import hdfsv.pkg1.HDFS.ReadBlockRequest;
import hdfsv.pkg1.HDFS.ReadBlockResponse;
import hdfsv.pkg1.HDFS.WriteBlockRequest;
import hdfsv.pkg1.HDFS.WriteBlockResponse;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Sravya
 */
public class DN implements IDataNode {

    static INameNode iDN = null;
    static byte[] input;
    static BlockReportRequest.Builder brreq = BlockReportRequest.newBuilder();

    public DN() throws RemoteException, NotBoundException {
        DataNodeLocation.Builder dn = DataNodeLocation.newBuilder();
        dn.setIp(0);
        dn.setPort(1099);
        HeartBeatRequest.Builder hbreq = HeartBeatRequest.newBuilder();
        hbreq.setId(id);
        input = hbreq.build().toByteArray();
        Registry reg = LocateRegistry.getRegistry("127.0.0.1", 1096);
        iDN = (INameNode) reg.lookup("namenode");
        System.out.println("connected to server");
        
        brreq.setLocation(dn);
        brreq.setId(id);        
          String directory = "C:/Users/Sravya/Documents/4-2/DS/Pro/HDFS/DN/";
            File folder = new File(directory);
            File[] listOfFiles = folder.listFiles();

            for (int i = 0; i < listOfFiles.length; i++) {

                if (listOfFiles[i].isFile()) {
                    
                    brreq.addBlockNumbers(Integer.parseInt(listOfFiles[i].getName()));
                }
            }
        func();
    }
    private int id = 0;

    @Override
    public byte[] readBlock(byte[] pro) {
        byte[] output = null;
        try {
            ReadBlockRequest rbr = ReadBlockRequest.parseFrom(pro);
            System.out.println(rbr.getBlockNumber());
            Path path;
            String file = "C:/Users/Sravya/Documents/4-2/DS/Pro/HDFS/files/Block" + Integer.toString(rbr.getBlockNumber()) + ".txt";
            System.out.println(file);
            path = Paths.get(file);
            try {
                byte[] data = Files.readAllBytes(path);
                System.out.println(data);
                ReadBlockResponse.Builder rbres = ReadBlockResponse.newBuilder();
                rbres.addData(ByteString.copyFrom(data));
                rbres.setStatus(rbr.getBlockNumber());
                output = rbres.build().toByteArray();
            } catch (IOException ex) {
                System.out.println("failopen");
                Logger.getLogger(DN.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println("yeah");

        } catch (InvalidProtocolBufferException ex) {
            System.out.println("Fail");
            Logger.getLogger(DN.class.getName()).log(Level.SEVERE, null, ex);
        }
        return output;
    }

    @Override
    public byte[] writeBlock(byte[] pro) {

        byte[] out = null;
        try {
            WriteBlockRequest wbreq = HDFS.WriteBlockRequest.parseFrom(pro);
            BlockLocations bl = wbreq.getBlockInfo();
            ByteString x = wbreq.getData(0);
            byte[] dat = x.toByteArray();
            System.out.println(bl.getBlockNumber() + "blockno.");
            String filename = "C:/Users/Sravya/Documents/4-2/DS/Pro/HDFS/files/Block" + Integer.toString(bl.getBlockNumber()) + ".txt";
            FileOutputStream output = new FileOutputStream("filename", true); //the true will append the new data
            output.write(dat);
            System.out.println(dat.toString() + "dataout");
            WriteBlockResponse.Builder wbres = WriteBlockResponse.newBuilder();
            wbres.setStatus(1);
            out = wbres.build().toByteArray();

        } catch (InvalidProtocolBufferException ex) {
            Logger.getLogger(DN.class.getName()).log(Level.SEVERE, null, ex);

        } catch (FileNotFoundException ex) {
            Logger.getLogger(DN.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DN.class.getName()).log(Level.SEVERE, null, ex);
        }
        return out;
    }

    public void HeartBeat() {

        return;
    }

    public static void blockrep()
    {
        
    }
    public static void func() {
        Runnable r;
        r = new Runnable() {
            public void run() {
                boolean flag = true;
                int i = 0;
                while (flag) {
                    i++;
                    if(i==20)
                    {
                        i=1;
                        byte[] out=iDN.blockReport(brreq.build().toByteArray());
                        
                    }

                    byte[] out = iDN.heartBeat(input);
                    try {
                        Thread.sleep(200000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        Thread t = new Thread(r);
        t.start();

    }
    public static void main(String args[]) {
        try {
            Registry reg = LocateRegistry.createRegistry(1099);
            reg.rebind("datanode",new DN());
            System.out.println("Server Started");
            
        }
        catch (Exception e){
            System.out.println(e+"EEEEEEE");
        }
        
    }
}
