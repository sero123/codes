package hdfsv.pkg1;


import com.google.protobuf.ByteString;
import hdfsv.pkg1.HDFS;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Admin
 */
public class RMIClient {

    public static void main(String args[]) {
        RMIClient client = new RMIClient();
        client.connectServer();

    }

    public void connectServer() {
        try {
            Registry reg = LocateRegistry.getRegistry("127.0.0.1", 1099);
            IDataNode iDN=null;
            iDN = (IDataNode) reg.lookup("datanode");
            System.out.println("connected to server");          
            HDFS.ReadBlockRequest.Builder rbreq = HDFS.ReadBlockRequest.newBuilder();
            rbreq.setBlockNumber(12);
            byte[] in = rbreq.build().toByteArray();
            byte[] out = iDN.readBlock(in);
            HDFS.ReadBlockResponse rbr = HDFS.ReadBlockResponse.parseFrom(out);
            System.out.println(rbr.getStatus());
            ByteString x = rbr.getData(0);
            System.out.println(x.toByteArray().toString());
            System.out.println("write");
            HDFS.WriteBlockRequest.Builder wbreq=HDFS.WriteBlockRequest.newBuilder();
       HDFS.BlockLocations.Builder bl = HDFS.BlockLocations.newBuilder();
       bl.setBlockNumber(12);
       HDFS.DataNodeLocation.Builder dnl = HDFS.DataNodeLocation.newBuilder();
       dnl.setIp(123);
       dnl.setPort(1099);
       bl.addLocations(dnl);
       wbreq.setBlockInfo(bl);
       String data="savysavy";
       wbreq.addData(ByteString.copyFrom(data.getBytes()));
       System.out.println(data.getBytes().toString()+"datain");
       out = iDN.writeBlock(wbreq.build().toByteArray());
       HDFS.WriteBlockResponse wbres = HDFS.WriteBlockResponse.parseFrom(out);
       System.out.println(wbres.getStatus()+"wbresponse");

        } catch (Exception e) {
            System.out.println(e);

        }
    }
}
