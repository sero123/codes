A1=imread('pic1.tif','TIFF');
A2=imread('pic2.tif','TIFF');
A3=imread('pic3.tif','TIFF');
A4=imread('pic4.tif','TIFF');
A5=imread('pic5.tif','TIFF');
A7=imread('pic7.tif','TIFF');
%{
A1 = imresize(A1, 0.5, 'bicubic');
A2 = imresize(A2, 0.5, 'bicubic');
A3 = imresize(A3, 0.5, 'bicubic');
A4 = imresize(A4, 0.5, 'bicubic');
A5 = imresize(A5, 0.5, 'bicubic');
A7 = imresize(A7, 0.5, 'bicubic');
%}

x=6401:6500;
y=6401:6500;
A1=A1(x,y);
A2=A2(x,y);
A3=A3(x,y);
A4=A4(x,y);
A5=A5(x,y);
A7=A7(x,y);

A1 = imresize(A1, 2, 'bicubic');
A2 = imresize(A2, 2, 'bicubic');
A3 = imresize(A3, 2, 'bicubic');
A4 = imresize(A4, 2, 'bicubic');
A5 = imresize(A5, 2, 'bicubic');
A7 = imresize(A7, 2, 'bicubic');

A1=single(A1);
A2=single(A2);
A3=single(A3);
A4=single(A4);
A5=single(A5);
A7=single(A7);

P=[A1(:),A2(:),A3(:),A4(:),A5(:),A7(:)];


col_add=sum(P);
total_sum=sum(col_add);
%P=P.*total_sum
%col_add=sum(P)
row_add=sum(P,2);
[m,n]=size(P);
yo=zeros(m,n);

for i=1:m
    for j=1:n
        val=row_add(i,1)*col_add(1,j);
        sq_root=sqrt(val);
        val=val/total_sum;
        u1=val/sq_root;
        u2=single(P(i,j));
        u2=u2/sq_root;
        yo(i,j)=u2-u1;        
    end
end

trans=transpose(yo);
help=trans*yo



P1=A1*help(1,1)+A2*help(1,2)+A3*help(1,3)+A4*help(1,4)+A5*help(1,5)+A7*help(1,6);
P2=A1*help(2,1)+A2*help(2,2)+A3*help(2,3)+A4*help(2,4)+A5*help(2,5)+A7*help(2,6);
P3=A1*help(3,1)+A2*help(3,2)+A3*help(3,3)+A4*help(3,4)+A5*help(3,5)+A7*help(3,6);
P4=A1*help(4,1)+A2*help(4,2)+A3*help(4,3)+A4*help(4,4)+A5*help(4,5)+A7*help(4,6);
P5=A1*help(5,1)+A2*help(5,2)+A3*help(5,3)+A4*help(5,4)+A5*help(5,5)+A7*help(5,6);
P7=A1*help(6,1)+A2*help(6,2)+A3*help(6,3)+A4*help(6,4)+A5*help(6,5)+A7*help(6,6);


min1=min(P1(:));
max1=max(P1(:));
para=(P1-min1)*1.0/(max1-min1);
para=para.*255;
para=uint8(para);
imwrite(para, 'intercomp1.tiff');

x=12801:13000;
y=12801:13000;


A8=imread('pic8.tif','TIFF');
A8=A8(x,y);
imwrite(A8,'actual.tiff');
A8=single(A8);
%A8 = imresize(A8, 0.5, 'bicubic');
%imwrite(A8, 'pan.tiff');

I=P7.^2;
Pan=A8.^2;
Pan=single(Pan);
mean1=mean(I(:));
mean2=mean(Pan(:));
std1=std(I(:));
std2=std(Pan(:));
stdval=std1*1.0/std2;
Pnew=(Pan-mean2+std2)*stdval ;
Pnew=Pnew+(mean1-std1);
P7=sqrtm(Pnew);
P7=abs(P7);

helpI=[9668.6747 ,-5036.1446, 78.3133 , 1662.6506 ,  921.6867  ,3403.6145;
-5036.1446 ,11050.6024  , 390.3614  ,1672.2892 , 3209.6386  ,-265.0602;
   78.3133   ,390.3614 , 3654.2169 , 2043.3735 , 2545.7831,  -259.0361;
 1662.6506 , 1672.2892 , 2043.3735,  3074.6988  , 2356.6265,  2192.7711;
  921.6867 , 3209.6386 , 2545.7831 , 2356.6265  , 5654.2169 , -740.9639;
 3403.6145 , -265.0602,  -259.0361 , 2192.7711  , -740.9639 , 4126.5060];

%helpI=[3229.3070,-3334.1293,-515.4984,-85.1805,115.5620,1008.5551;-3089.9121,6136.2547,-1630.2520 ,-70.3747,72.6849,12.7911;-54.1993,-1445.7323,1532.3451,-68.5715,49.9802,-737.8585;-60.7343,-42.9950,-35.3247,-87.5848,-5.6801,-111.7909;921.4297,-104.1831,-892.9816,-113.7322,323.5385,772.6281;-17.0836,-76.0218,-130.6892,-5.6945,-263.2291,313.0561]

B1=P1*helpI(1,1)+P2*helpI(1,2)+P3*helpI(1,3)+P4*helpI(1,4)+P5*helpI(1,5)+P7*helpI(1,6);
B2=P1*helpI(2,1)+P2*helpI(2,2)+P3*helpI(2,3)+P4*helpI(2,4)+P5*helpI(2,5)+P7*helpI(2,6);
B3=P1*helpI(3,1)+P2*helpI(3,2)+P3*helpI(3,3)+P4*helpI(3,4)+P5*helpI(3,5)+P7*helpI(3,6);
B4=P1*helpI(4,1)+P2*helpI(4,2)+P3*helpI(4,3)+P4*helpI(4,4)+P5*helpI(4,5)+P7*helpI(4,6);
B5=P1*helpI(5,1)+P2*helpI(5,2)+P3*helpI(5,3)+P4*helpI(5,4)+P5*helpI(5,5)+P7*helpI(5,6);
B7=P1*helpI(6,1)+P2*helpI(6,2)+P3*helpI(6,3)+P4*helpI(6,4)+P5*helpI(6,5)+P7*helpI(6,6);

%{
A1(1,1)
B1(1,1)
help*helpI
%}








%{
B1(1,1)
B2(1,1)
p1=[min(B1(:)),min(B2(:)),min(B3(:)),min(B4(:)),min(B5(:)),min(B7(:))];
minf=min(p1)
p2=[max(B1(:)),max(B2(:)),max(B3(:)),max(B4(:)),max(B5(:)),max(B7(:))];
maxf=max(p2)
B1=(B1-minf)/(maxf-minf);
B1=B1.*255;
B1=uint8(B1);

%imwrite(B1, 'du11.tiff');
B2=(B2-minf)/(maxf-minf);
B2=B2.*255;
B2=uint8(B2);
B2(2,4)
%imwrite(B2, 'du12.tiff');
B3=(B3-minf)/(maxf-minf);
B3=B3.*255;
B3=uint8(B3);
%imwrite(B3, 'du13.tiff');
B4=(B4-minf)/(maxf-minf);
B4=B4.*255;
B4=uint8(B4);
%imwrite(B4, 'du14.tiff');
%}



min1=min(B1(:));
min2=min(B2(:));
B1=(B1-min1)*1.0/(max1-min1);
B1=B1.*255;
B1=uint8(B1);
imwrite(B1, 'du11.tiff');

min1=min(B2(:));
max1=max(B2(:));
B2=(B2-min1)*1.0/(max1-min1);
B2=B2.*255;
B2=uint8(B2);

imwrite(B2, 'du12.tiff');
min1=min(B3(:));
max1=max(B3(:));
B3=(B3-min1)*1.0/(max1-min1);
B3=B3.*255;
B3=uint8(B3);
imwrite(B3, 'du13.tiff');
min1=min(B4(:));
max1=max(B4(:));
B4=(B4-min1)*1.0/(max1-min1);
B4=B4.*255;
B4=uint8(B4);
imwrite(B4, 'du14.tiff');
min1=min(B5(:));
max1=max(B5(:));
B5=(B5-min1)*1.0/(max1-min1);
B5=B5.*255;
B5=uint8(B5);
imwrite(B5, 'du15.tiff');
min1=min(B7(:));
max1=max(B7(:));
B7=(B7-min1)*1.0/(max1-min1);
B7=B7.*255;
B7=uint8(B7);
imwrite(B7, 'du17.tiff');

