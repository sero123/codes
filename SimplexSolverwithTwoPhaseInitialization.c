#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int main() 
{
	int n,i,j,nosurplus=0,maxcol,minrow;
	float ratio,minratio,val,maxval;
	char buf[4];
	int a,b;
	scanf("%s",buf);
	//printf("buf = %s\n",buf);
	scanf("%d",&n);
	//printf("n = %d\n",n);
	scanf("%d%d",&a,&b);
	//printf("a = %d\n",a);
	//printf("b = %d\n",b);
	if(strcmp(buf,"MIN")==0)
	{
	//	printf("min\n");
		a=-a;
		b=-b;
	}
	int inter[n][3],initial[2+n];
	initial[0]=a;
	initial[1]=b;
	for(i=2;i<n+2;i++)
		initial[i]=0;
	int s[n];
	for(i=0;i<n;i++)
	{
		scanf("%d%d%d",&inter[i][0],&inter[i][1],&inter[i][2]);
		if(inter[i][2]<0)
		{
			s[i]=1;
			inter[i][0]=-inter[i][0];
			inter[i][1]=-inter[i][1];
			inter[i][2]=-inter[i][2];
			nosurplus++;
	//	printf("%d %d %d\n",inter[i][0],inter[i][1],inter[i][2]);
		}
		else
			s[i]=0;
	}
	int basin[n], basval[n];
	for(i=0;i<n;i++)
	{
		basin[i]=2+i;
		basval[i]=0;
	}

	if(!nosurplus)
	{
		float tab[n+1][n+3],getval,getfactor;
		for(i=0;i<n;i++)
		{
			tab[i][0]=inter[i][0];
			tab[i][1]=inter[i][1];
			tab[i][n+2]=inter[i][2];
			for(j=2;j<n+2;j++)
				tab[i][j]=0;
			tab[i][2+i]=1;		
		}
		for(i=0;i<n+3;i++)
			tab[n][i]=0;
		while(1) 
		{
		
			//count++;
			
	/*		for(i=0;i<n+3;i++)
				printf("%f ",tab[n][i]);
			printf("\n");*/
	/*		tab[n][n+2]=val;
			for(j=0;j<n+2;j++)
				if(tab[n][j]>0)
					break;
			if(j==n+2)
			{
				break;
			}*/
			for(i=0;i<n+2;i++)
			{
				val=0;
				for(j=0;j<n;j++)
					val+=basval[j]*tab[j][i];
				tab[n][i]=initial[i]-val;
			}
			val=0;
			//printf("basvals\n");
			//for(j=0;j<n;j++)
			//	printf("%d ",basval[j]);
			//printf("\n");
			//printf("between vals\n");
			for(j=0;j<n;j++)
			{
			//	printf("%d %f\n",basval[j],tab[j][n+2]);
				val+=basval[j]*tab[j][n+2];
			}
			tab[n][n+2]=val;
	//		printf("1st phase\n");
	//		printf("total table\n");
/*			for(i=0;i<n+1;i++)
			{
				for(j=0;j<n+3;j++)
				{
					printf("%f ",tab[i][j]);
				}
				printf("\n");
			}*/
			for(j=0;j<n+2;j++)
				if(tab[n][j]>0)
				{
			//		printf("are ?\n");
					break;
				}
			if(j==n+2)
			{
				int fl1=-1,fl2=-1;
				for(i=0;i<n;i++)
				{
					if(basin[i]==0)
					{
						fl1=i;
					}
					else if(basin[i]==1)
					{
						fl2=i;
					}
				}
				if(fl1!=-1)
					printf("%f ",tab[fl1][n+2]);
				else
					printf("0 ");
				if(fl2!=-1)
					printf("%f\n",tab[fl2][n+2]);
				else
					printf("0\n");
				
				printf("%f\n",tab[n][n+2]);
			//	printf("what?\n");
				break;
			}
			minratio=99999999;
			minrow=-1;
			maxcol=-1;
			maxval=-1;
			for(j=0;j<n+2;j++)
			{
				if(tab[n][j]>maxval)
				{
					maxval=tab[n][j];
					maxcol=j;
				}
			}
			for(i=0;i<n;i++)
			{
				ratio=tab[i][n+2]*1.0/tab[i][maxcol];
				if(ratio>0&&ratio<minratio)
				{
					minratio=ratio;
					minrow=i;
				}
				
			}
			if(minrow==-1)
			{
				printf("INFEASIBLE\n");
				exit(1);
			}
			basin[minrow]=maxcol;
			basval[minrow]=initial[maxcol];
			getval=tab[minrow][maxcol];
			if(getval!=1)
			{
				for(j=0;j<n+3;j++)
					tab[minrow][j]/=getval;
			}
			for(i=0;i<n;i++)
			{
				if(i==minrow)
					continue;
				getfactor=tab[i][maxcol];
				for(j=0;j<n+3;j++)
				{

					tab[i][j]=tab[i][j]-tab[minrow][j]*getfactor;
				}
			}
		}
	}
	else 
	{	
		//printf("dfkjfjf\n");
		int bassval2[n];
		float tab[n+1][n+3+nosurplus],getval,getfactor;
		
		for(i=0;i<n;i++)
		{
			tab[i][0]=inter[i][0];
			tab[i][1]=inter[i][1];
			tab[i][n+2+nosurplus]=inter[i][2];
			for(j=2;j<n+2+nosurplus;j++)
				tab[i][j]=0;
			if(s[i])
			 tab[i][2+i]=-1;
			else
			tab[i][2+i]=1;		
		}
		int k=0;
		for(i=0;i<n;i++)
		{
			if(s[i])
			{
				tab[i][2+n+k]=1;
				tab[n][2+n+k]=-1;
				k++;
			}
		}
		for(i=0;i<n+2;i++)
			tab[n][i]=0;

		int init[3+n+nosurplus];
		int bassin[n],bassval[n];
		for(i=0;i<3+n+nosurplus;i++)
			init[i]=0;
		k=0;
		for(i=0;i<n;i++)
		{
			if(s[i])
				bassin[i]=2+n+k;
			else bassin[i]=2+i;
			bassval[i]=0;
			bassval2[i]=0;
			k++;
		}
		for(j=0;j<3+n+nosurplus;j++)
			for(i=0;i<n+1;i++)
				if(s[i])
					init[j]+=tab[i][j];
		float interm[3+n+nosurplus], init2[3+n+nosurplus];
		interm[0]=initial[0];
		interm[1]=initial[1];
		init2[0]=initial[0];
		init2[1]=initial[1];
		for(i=2;i<n+3+nosurplus;i++)
		{
			interm[i]=0;
			init2[i]=0;
		}
	//	for(i=0;i<n+3+nosurplus;i++)
		//	printf("%d ",init[i]);
		//printf("\n");
		while(1) 
		{

/*			printf("1st phase\n");
			for(i=0;i<n+1;i++)
			{
				for(j=0;j<n+3+nosurplus;j++)
				{
					printf("%f ",tab[i][j]);
				}
				printf("\n");
			}
			for(i=0;i<n+3+nosurplus;i++)
				printf("%f ",interm[i]);
			printf("\n");*/
			//count++;
			
		//	for(i=0;i<n+3;i++)
		//		printf("%f ",tab[n][i]);
		//	printf("\n");
			float val1;
			for(i=0;i<n+2+nosurplus;i++)
			{
				val=0;
				val1=0;
				for(j=0;j<n;j++)
				{
					val+=bassval[j]*tab[j][i];
					val1+=bassval2[j]*tab[j][i];
				}
				tab[n][i]=init[i]-val;
				interm[i]=init2[i]-val1;
			}
			val=0;
		//	printf("basin\n");
			//for(j=0;j<n;j++)
				//printf("%d ",bassin[j]);
			//printf("\n");
			//printf("between vals\n");
			val1=0;
			for(j=0;j<n;j++)
			{
			//	printf("%d %f\n",basval[j],tab[j][n+2]);
				val+=bassval[j]*tab[j][n+2+nosurplus];
				val1+=bassval2[j]*tab[j][n+2+nosurplus];
			}
			tab[n][n+2+nosurplus]=val;
			interm[n+2+nosurplus]=val1;
			//printf("init2\n");
			//for(i=0;i<n+3+nosurplus;i++)
				//printf("%f ",init2[i]);
			//printf("\n");
			//printf("1st phase\n");
		/*	for(i=0;i<n+1;i++)
			{
				for(j=0;j<n+3+nosurplus;j++)
				{
					//printf("%f ",tab[i][j]);
				}
				//printf("\n");
			}*/
		//	for(i=0;i<n+3+nosurplus;i++)
				//printf("%f ",interm[i]);
			//printf("\n");
		/*	for(j=0;j<n+2+nosurplus;j++)
				if(tab[n][j]>0)
					break;*/
/*			if(j==n+2+nosurplus)
			{
				break;
			}*/
			int flag=0;
			for(i=0;i<n;i++)
				if(bassin[i]>=n+2)
				{
				//	printf("kjdkjdf\n");
					flag=1;
					break;
				}
			if(flag==0)
				break;
			minratio=99999999;
			minrow=-1;
			maxcol=-1;
			maxval=-1;
			for(j=0;j<n+2+nosurplus;j++)
			{
				if(tab[n][j]>maxval)
				{
					maxval=tab[n][j];
					maxcol=j;
				}
			}
			for(i=0;i<n;i++)
			{
				ratio=tab[i][n+2+nosurplus]*1.0/tab[i][maxcol];
				if(ratio>0&&ratio<minratio)
				{
					minratio=ratio;
					minrow=i;
				}
				else if(ratio>0&&ratio==minratio)
				{
					if(bassin[i]>=n+2){
						minratio=ratio;
						minrow=i;
					}
					
				}
			}
			if(minrow==-1)
			{
				printf("INFEASIBLE\n");
				exit(1);
			}
			bassin[minrow]=maxcol;
			bassval[minrow]=init[maxcol];
			bassval2[minrow]=init2[maxcol];
			getval=tab[minrow][maxcol];
			if(getval!=1)
			{
				for(j=0;j<n+3+nosurplus;j++)
					tab[minrow][j]/=getval;
			}
			for(i=0;i<n;i++)
			{
				if(i==minrow)
					continue;
				getfactor=tab[i][maxcol];
				for(j=0;j<n+3+nosurplus;j++)
				{

					tab[i][j]=tab[i][j]-tab[minrow][j]*getfactor;
				}
			}
			

		}
		for(i=0;i<n+2;i++)
			initial[i]=interm[i];
		for(i=0;i<n+2;i++)
			tab[n][i]=interm[i];
		for(i=0;i<n;i++)
			basval[i]=initial[bassin[i]];
		for(i=0;i<n;i++)
			basin[i]=bassin[i];
		for(i=0;i<n+1;i++)
			tab[i][2+n]=tab[i][2+n+nosurplus];
		tab[n][n+2]=interm[n+2+nosurplus];
		while(1) 
		{
/*			for(i=0;i<n+1;i++)
			{
				for(j=0;j<n+3;j++)
				{
					printf("%f ",tab[i][j]);
				}
				printf("\n");
			}*/
			for(j=0;j<n+2;j++)
				if(tab[n][j]>0)
					break;
			if(j==n+2)
			{
				int fl1=-1,fl2=-1;
				for(i=0;i<n;i++)
				{
					if(basin[i]==0)
					{
						fl1=i;
					}
					else if(basin[i]==1)
					{
						fl2=i;
					}
				}
				if(fl1!=-1)
					printf("%f ",tab[fl1][n+2]);
				else
					printf("0 ");
				if(fl2!=-1)
					printf("%f\n",tab[fl2][n+2]);
				else
					printf("0\n");

				printf("%f\n",tab[n][n+2]);
				break;
			}
		/*	for(i=0;i<n+1;i++)
			{
				for(j=0;j<n+3;j++)
				{
					printf("%f ",tab[i][j]);
				}
				printf("\n");
			}*/
			for(i=0;i<n+2;i++)
			{
				val=0;
				for(j=0;j<n;j++)
					val+=basval[j]*tab[j][i];
				tab[n][i]=initial[i]-val;
			}
			val=0;
			//printf("basvals\n");
			//for(j=0;j<n;j++)
			//	printf("%d ",basval[j]);
			//printf("\n");
			//printf("between vals\n");
			//			printf("basvals\n");
/*			for(j=0;j<n;j++)
				printf("%d ",basval[j]);
			printf("\n");*/
			//printf("between vals\n");
			for(j=0;j<n;j++)
			{
			//	printf("%d %f\n",basval[j],tab[j][n+2]);
				val+=basval[j]*tab[j][n+2];
			}
			
			tab[n][n+2]=val;
			for(j=0;j<n+2;j++)
				if(tab[n][j]>0)
					break;
			if(j==n+2)
			{
				int fl1=-1,fl2=-1;
				for(i=0;i<n;i++)
				{
					if(basin[i]==0)
					{
						fl1=i;
						break;
					}
					else if(basin[i]==1)
					{
						fl2=i;
						break;
					}
				}
				if(fl1!=-1)
					printf("%f ",tab[fl1][n+2]);
				else
					printf("0 ");
				if(fl2!=-1)
					printf("%f\n",tab[fl2][n+2]);
				else
					printf("0\n");

				printf("%f\n",tab[n][n+2]);
				break;
			}
			minratio=99999999;
			minrow=-1;
			maxcol=-1;
			maxval=-1;
			for(j=0;j<n+2;j++)
			{
				if(tab[n][j]>maxval)
				{
					maxval=tab[n][j];
					maxcol=j;
				}
			}
			for(i=0;i<n;i++)
			{
				ratio=tab[i][n+2]*1.0/tab[i][maxcol];
				if(ratio>0&&ratio<minratio)
				{
					minratio=ratio;
					minrow=i;
				}
			}
			if(minrow==-1)
			{
				printf("INFEASIBLE\n");
				exit(1);
			}
			basin[minrow]=maxcol;
			basval[minrow]=initial[maxcol];
			getval=tab[minrow][maxcol];
			if(getval!=1)
			{
				for(j=0;j<n+3;j++)
					tab[minrow][j]/=getval;
			}
			for(i=0;i<n;i++)
			{
				if(i==minrow)
					continue;
				getfactor=tab[i][maxcol];
				for(j=0;j<n+3;j++)
				{

					tab[i][j]=tab[i][j]-tab[minrow][j]*getfactor;
				}
			}
		}
	}
	
		
	return 0;
}
			


















