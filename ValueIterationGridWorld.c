#include<stdio.h>
#include<stdlib.h>
#include<math.h>

float prob1, prob2, prob3, prob4;

float func(int i, int drns[][4], int a, float val[])
{
	int p,q,r;
	if(a==0)
	{
//		printf("left : %0.03f ", val[drns[i][0]]*prob1+val[drns[i][2]]*prob2+val[drns[i][3]]*prob3+val[drns[i][1]]*prob4);

	return val[drns[i][0]]*prob1+val[drns[i][2]]*prob2+val[drns[i][3]]*prob3+val[drns[i][1]]*prob4;
	}
	else if(a==1) {
//		printf("right : %0.03f ", val[drns[i][1]]*prob1+val[drns[i][2]]*prob2+val[drns[i][3]]*prob3+val[drns[i][0]]*prob4);
	return val[drns[i][1]]*prob1+val[drns[i][2]]*prob2+val[drns[i][3]]*prob3+val[drns[i][0]]*prob4;
	}
	else if(a==2) {
//		printf("up : %0.03f ", val[drns[i][2]]*prob1+val[drns[i][0]]*prob2+val[drns[i][1]]*prob3+val[drns[i][3]]*prob4);
	return val[drns[i][2]]*prob1+val[drns[i][0]]*prob2+val[drns[i][1]]*prob3+val[drns[i][3]]*prob4;
	}
	else if(a==3)
	{
//		printf("down : %0.03f\n",val[drns[i][3]]*prob1+val[drns[i][0]]*prob2+val[drns[i][1]]*prob3+val[drns[i][2]]*prob4);
	return val[drns[i][3]]*prob1+val[drns[i][0]]*prob2+val[drns[i][1]]*prob3+val[drns[i][2]]*prob4;
	}
}

int main()
{
	int i,j,k;
	int n;
	int drns[n][4];
	int in,fi,si;
	float gamma, reval, rew[n], val[n], sum;
	float theta,max;
	prob1=0.8;
	prob2=0.1;
	prob3=0.1;
	prob4=0;
//	printf("Enter n,in,fi,sink,reward\n");
	scanf("%d%d%d%d%f",&n,&in,&fi,&si,&reval);
//	printf("Enter gamma\n");
	scanf("%f",&gamma);
	for(i=0;i<n;i++)
		rew[i]=reval;
//	printf("Enter drns\n");
	for(i=0;i<n;i++)
		for(j=0;j<4;j++)
			scanf("%d",&drns[i][j]);
	int flag;
//	printf("Enter theta\n");
	scanf("%f",&theta);

	for(i=0;i<n;i++)
		val[i]=0;

	rew[fi]=val[fi]=45; //final position value=1;
	rew[si]=val[si]=-45;
	float valn[n];
	int action[n], act;
	int cnt=0;
	while(1) {
		cnt++;
		printf(" **************** iteration %d *****************\n",cnt);
		valn[fi]=val[fi];
		valn[si]=val[si];
		for(i=0;i<n;i++) {
			if(i==fi||i==si)
				continue;
			printf(" state %d\n",i);
			printf("\n");
			printf("U(t-1) = %0.03f\n",val[i]);
			max=-9999;
			for(j=0;j<4;j++) {
				sum=func(i, drns, j, val);
				if (sum>max)
					max=sum;
			}
			printf("choosing trans*preval = %0.03f\n",max);
			printf("U(t) = %0.03f\n",rew[i]+max);
			printf("\n");
			valn[i]=rew[i]+max;
		}
		flag=0;
		
		for(i=0;i<n;i++)
		{
			printf("%d %f %f\n",i,valn[i],val[i]);
			if(fabs(valn[i]-val[i])>theta)
			{
				flag=1;
				break;
			}
		}
		if(flag==0)
		{
			printf("reached\n");
			break;
		}

		for(i=0;i<n;i++)
		{
			val[i]=valn[i];
			//printf("%f ",valn[i]);
		}
	//	printf("\n");
	}
	for(i=0;i<n;i++)
		printf("%f ",valn[i]);
	printf("\n");
	action[fi]=-1;
	action[si]=-1;
	for(i=0;i<n;i++) {
		if(i==fi||i==si)
			continue;
		max=-9999;
		for(j=0;j<4;j++) {
			sum=func(i, drns, j, valn);
			if (sum>max)
			{
				max=sum;
				action[i]=j;
			}
		}
		val[i]=rew[i]+max;
	}
	for(i=0;i<n;i++)
		printf("%d ",action[i]);
	printf("\n");
	return 0;
}








