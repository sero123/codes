/* udpserver.c 
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>*/
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/sendfile.h>
#include <sys/stat.h>
#include<ctype.h>
#include<regex.h>
struct packet
{
	//int ack;
	//int last;
	//char IP[15];
	char str[1002];
};
typedef struct packet packet;
packet *send_data;
packet *recv_data;
char string[40000],r_data[10][1024];
#define STR_VALUE(val) #val
#define STR(name) STR_VALUE(name)
#define PATH_LEN 256
#define MD5_LEN 32
int flagq=0;
int flagreg=0,short_list=0;
char patt[100],date2[100],date3[100];
time_t to_seconds(const char *date)
{
	struct tm storage={0,0,0,0,0,0,0,0,0};
	char *p=NULL;
	time_t retval=0;

	p=(char *)strptime(date,"%d-%b-%Y",&storage);
	if(p==NULL)
	{
		retval=0;
	}
	else
	{
		retval=mktime(&storage);
	}
	return retval;
}
int CalcFileMD5(char *file_name, char *md5_sum)
{
#define MD5SUM_CMD_FMT "md5sum %." STR(PATH_LEN) "s 2>/dev/null"
	char cmd[PATH_LEN + sizeof (MD5SUM_CMD_FMT)];
	sprintf(cmd, MD5SUM_CMD_FMT, file_name);
#undef MD5SUM_CMD_FMT
	FILE *p = popen(cmd, "r");
	if (p == NULL) return 0;
	int i, ch;
	for (i = 0; i < MD5_LEN && isxdigit(ch = fgetc(p)); i++) {
		*md5_sum++ = ch;
	}
	*md5_sum = '\0';
	pclose(p);
	return i == MD5_LEN;
}
void CheckAll()
{
	int Fp,con=1,m=0,n=0;
	char buf[2];
	char file[100];
	char md5[MD5_LEN + 1];
	struct stat sb;
	Fp=open("temp1",O_RDONLY);
	while(1)
	{
		//printf("while\n");
		con=read(Fp,buf,1);
		if(con==0)
			break;
		if(buf[0]=='\n')
		{
			file[m]='\0';

			if (!CalcFileMD5(file, md5))
			{
				puts("Error occured!");
			}
			else
			{
				//printf("Success! MD5 sum is: %s\n", md5);
				strcat(string,md5);
				//strcat(,md5);
			}
			strcat(string," ");
			strcat(string,file);
			strcat(string," ");
			stat(file,&sb);
			strcat(string, ctime(&sb.st_mtime));
			strcat(string,"\n");
			m=0;
			n=n+1;
		}
		else
		{
			file[m]=buf[0];
			m++;
		}
	}
}
int match(char text[],char pattern[])
{
	regex_t    preg;
	int        rc;
	size_t     nmatch = 2;
	regmatch_t pmatch[2];

	if (0 != (rc = regcomp(&preg, pattern, 0))) {
		//printf("regcomp() failed, returning nonzero (%d)\n", rc);
		//exit(EXIT_FAILURE);
		return -10;
	}

	if (0 != (rc = regexec(&preg, text, nmatch, pmatch, 0))) {
		//      printf("Failed to match '%s' with '%s',returning %d.\n",
		//    text, pattern, rc);
		return -10;
	}
	else {
		/*      printf("With the whole expression, "
			"a matched substring \"%.*s\" is found at position %d to %d.\n",
			pmatch[0].rm_eo - pmatch[0].rm_so, &text[pmatch[0].rm_so],
			pmatch[0].rm_so, pmatch[0].rm_eo - 1);
			printf("With the sub-expression, "
			"a matched substring \"%.*s\" is found at position %d to %d.\n",
			pmatch[1].rm_eo - pmatch[1].rm_so, &text[pmatch[1].rm_so],
			pmatch[1].rm_so, pmatch[1].rm_eo - 1);*/
		return 1;
	}
	regfree(&preg);

}
char * change_time(char *new,char *old)
{
	char delims[] = " ";
	char *parts = NULL;
	parts = strtok( old, delims );
	char r_date[20][2024];
	int d_cnt=0,j;
	while( parts != NULL )
	{
		strcpy(r_date[d_cnt],parts);
		parts = strtok( NULL, delims );
		d_cnt++;
	}
	strcpy(new,r_date[3]);
	//printf("date1 is %s\n",date1);
	return new;
}
char * change(char *new,char *old)
{
	char delims[] = " ";
	char *parts = NULL;
	parts = strtok( old, delims );
	char r_date[20][2024];
	int d_cnt=0,j;
	while( parts != NULL )
	{
		strcpy(r_date[d_cnt],parts);
		parts = strtok( NULL, delims );
		d_cnt++;
	}
	strcpy(new,r_date[2]);
	strcat(new,"-");
	strcat(new,r_date[1]);
	strcat(new,"-");
	strcat(new,r_date[4]);
	// printf("date1 is %s\n",date1);
	return new;
}
void file_info(char *file)
{
	struct stat sb;
	stat(file, &sb);
	char  date1[1024];
	char  date2c[1024];
	char  date3c[1024];
	char  date1t[1024];
	char  date2t[1024];
	char  date3t[1024];
	char  date33[1024];
	char  date22[1024];
	strcpy(date22,date2);
	strcpy(date33,date3);
	strcpy(date1,change(date1,ctime(&sb.st_mtime)));
	strcpy(date2c,change(date2c,date22));
	strcpy(date3c,change(date3c,date33));
	strcpy(date1t,change_time(date1t,ctime(&sb.st_mtime)));
	strcpy(date2t,change_time(date2t,date22));
	strcpy(date3t,change_time(date3t,date33));
	time_t d1=to_seconds(date1);
	time_t d2=to_seconds(date2c);
	time_t d3=to_seconds(date3c);

	printf("date comparison: %s %s ",date1,date2);
	int flag=0;
	if(short_list==1)
	{
		if(d1 > d2 && d1 <d3 )
		{
			flag=1;
		}
		if(d2==d3)
		{
			if(strcmp(date1t,date2t)>0 && strcmp(date1t,date3t)<0)
				flag=1;
		}
		if(flag==1)
		{
			//printf("File Name:%s\n",file);
			strcat(string,file);
			strcat(string,"File type:                ");
			switch (sb.st_mode & S_IFMT) {
				case S_IFBLK:  strcat(string,"block device\n");            break;
				case S_IFCHR:  strcat(string,"character device\n");        break;
				case S_IFDIR:  strcat(string,"directory\n");               break;
				case S_IFIFO:  strcat(string,"FIFO/pipe\n");               break;
				case S_IFLNK:  strcat(string,"symlink\n");                 break;
				case S_IFREG:  strcat(string,"regular file\n");            break;
				case S_IFSOCK: strcat(string,"socket\n");                  break;
				default:       strcat(string,"unknown?\n");                break;
			}

			/*printf("I-node number:            %ld\n", (long) sb.st_ino);
			  printf("Mode:                     %lo (octal)\n",
			  (unsigned long) sb.st_mode);

			  printf("Link count:               %ld\n", (long) sb.st_nlink);
			  printf("Ownership:                UID=%ld   GID=%ld\n",
			  (long) sb.st_uid, (long) sb.st_gid);

			  printf("Preferred I/O block size: %ld bytes\n",
			  (long) sb.st_blksize);
			  printf("File size:                %lld bytes\n",
			  (long long) sb.st_size);

			  printf("Blocks allocated:         %lld\n",
			  (long long) sb.st_blocks);

			  printf("Last status change:       %s", ctime(&sb.st_ctime));
			  printf("Last file access:         %s", ctime(&sb.st_atime));
			  printf("Last file modification:   %s", ctime(&sb.st_mtime));*/
			strcat(string,ctime(&sb.st_mtime));


			//exit(EXIT_SUCCESS);
		}
		else
			printf("no such files");
	}
	else
	{
		//printf("File Name:%s\n",file);
		strcat(string,file);
		strcat(string,"File type:                ");
		switch (sb.st_mode & S_IFMT) {
			case S_IFBLK:  strcat(string,"block device\n");            break;
			case S_IFCHR:  strcat(string,"character device\n");        break;
			case S_IFDIR:  strcat(string,"directory\n");               break;
			case S_IFIFO:  strcat(string,"FIFO/pipe\n");               break;
			case S_IFLNK:  strcat(string,"symlink\n");                 break;
			case S_IFREG:  strcat(string,"regular file\n");            break;
			case S_IFSOCK: strcat(string,"socket\n");                  break;
			default:       strcat(string,"unknown?\n");                break;
		}
		/*printf("I-node number:            %ld\n", (long) sb.st_ino);
		  printf("Mode:                     %lo (octal)\n",
		  (unsigned long) sb.st_mode);
		  printf("Link count:               %ld\n", (long) sb.st_nlink);
		  printf("Ownership:                UID=%ld   GID=%ld\n",
		  (long) sb.st_uid, (long) sb.st_gid);

		  printf("Preferred I/O block size: %ld bytes\n",
		  (long) sb.st_blksize);
		  printf("File size:                %lld bytes\n",
		  (long long) sb.st_size);
		  printf("Blocks allocated:         %lld\n",
		  (long long) sb.st_blocks);

		  printf("Last status change:       %s", ctime(&sb.st_ctime));
		  printf("Last file access:         %s", ctime(&sb.st_atime));
		  printf("Last file modification:   %s", ctime(&sb.st_mtime));*/
		//char buffer[256];
		//printf ("Enter a long number: ");
		// fgets (buffer, 256, stdin);
		//strcat(string,buffer);
		strcat(string,ctime(&sb.st_mtime));
	}
}
void listdir(const char *name, int level)
{
	DIR *dir;
	int s;
	struct dirent *entry;

	if (!(dir = opendir(name)))
		return;
	if (!(entry = readdir(dir)))
		return;

	do {
		if (entry->d_type == DT_DIR) {
			char path[1024];
			int len = snprintf(path, sizeof(path)-1, "%s/%s", name, entry->d_name);
			path[len] = 0;
			if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
				continue;
			if(flagreg==1)
			{
				if(match(entry->d_name,patt)!=-10 )
				{
					file_info(entry->d_name);
					strcat(string,"\n");
					listdir(path, level + 1);
				}
			}
			else
			{
				file_info(entry->d_name);
				strcat(string,"\n");
				listdir(path, level + 1);
			}
		}
		else
		{

			if(flagreg==1)
			{
				if(match(entry->d_name,patt)!=-10)
				{
					file_info(entry->d_name);
					strcat(string,"\n");
				}
			}
			else
			{
				file_info(entry->d_name);
				strcat(string,"\n");
			}
		}
	} while (entry = readdir(dir));
	closedir(dir);
}
int main()
{
	string[0]='\0';	
	send_data= malloc(sizeof(packet));
	recv_data= malloc(sizeof(packet));
	int sock;
	int addr_len, bytes_read;
	//char recv_data[1024];
	//char send_data[1024];
	struct sockaddr_in server_addr , client_addr;


	if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
		perror("Socket");
		exit(1);
	}

	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(5000);
	server_addr.sin_addr.s_addr = INADDR_ANY;
	if (bind(sock,(struct sockaddr *)&server_addr,
				sizeof(struct sockaddr)) == -1)
	{
		perror("Bind");
		exit(1);
	}
	addr_len = sizeof(struct sockaddr);
	printf("\nUDPServer Waiting for client on port 5001");
	fflush(stdout);
	while (1)
	{
		string[0]='\0';
		bytes_read = recvfrom(sock,recv_data,1024,0,(struct sockaddr *)&client_addr, &addr_len);
		recv_data->str[bytes_read] = '\0';
		printf("\n(%s , %d) said : ",inet_ntoa(client_addr.sin_addr),ntohs(client_addr.sin_port));
		printf("%s", recv_data->str);
		char delims[] = " ";
		char *parts = NULL;
		parts = strtok( recv_data->str, delims );
		int recv_cnt=0,j;
		while( parts != NULL ) 
		{
			strcpy(r_data[recv_cnt],parts);
			parts = strtok( NULL, delims );
			recv_cnt++;
		}
		if(strcmp(r_data[0],"FileHash")==0 && strcmp(r_data[1],"Verify")==0)
		{
			char Buff[100];
			char md5[MD5_LEN + 1];
			if (!CalcFileMD5(r_data[2], md5)) 
			{
				puts("Error occured!");
			} 
			else 
			{
				strcpy(send_data->str,md5);
			}
			strcat(send_data->str,r_data[2]);
			strcat(send_data->str,"\n");
			struct stat sb;
			stat(r_data[2],&sb);
			strcat(send_data->str, ctime(&sb.st_mtime));
			int k=1;
		while(k!=0)
		{
			sendto(sock, send_data, strlen(send_data->str), 0,(struct sockaddr *)&client_addr, sizeof(struct sockaddr));
			bytes_read = recvfrom(sock,recv_data,1024,0,(struct sockaddr *)&client_addr, &addr_len);
			recv_data->str[bytes_read] = '\0';
			if(strcmp(recv_data->str,"Ack")==0)
			{
				printf("Ack received\n");
				k=0;
			}
		}
		}
		else if(strcmp(r_data[0],"FileHash")==0 && strcmp(r_data[1],"CheckAll")==0)
		{
			system("ls $(find -type f) > temp1");
			CheckAll();
			if(strlen(string)<=1000)
			{
				int k=1;
				strcpy(send_data->str,string);
				while(k!=0)
				{
					sendto(sock, send_data, strlen(send_data->str), 0,(struct sockaddr *)&client_addr, sizeof(struct sockaddr));

					bytes_read = recvfrom(sock,recv_data,1024,0,(struct sockaddr *)&client_addr, &addr_len);
					recv_data->str[bytes_read] = '\0';
					if(strcmp(recv_data->str,"Ack")==0)
					{
						printf("Ack received\n");
						k=0;
					}
				}
			}
			else if(strlen(string)>1000)
			{
				int j=strlen(string)/1000;
				printf("j is %d\n",j);
				int k=0;
				int p;
				int l,i;
				int count=1;
				while(j>=0)
				{
					l=0;
					for(i=k;i<k+1000;i++)
					{
						send_data->str[l]=string[i];
						l++;
					}
					count=1;
					while(count!=0)
					{
						sendto(sock, send_data, strlen(send_data->str), 0,(struct sockaddr *)&client_addr, sizeof(struct sockaddr));

						bytes_read = recvfrom(sock,recv_data,1024,0,(struct sockaddr *)&client_addr, &addr_len);
						recv_data->str[bytes_read] = '\0';
						if(strcmp(recv_data->str,"Ack")==0)
						{
							printf("Ack received\n");
							count=0;
						}
					}
					j--;
					k=k+1000;
				}
			}
			int count=1;
			while(count!=0)
			{
				strcpy(send_data->str,"Hi");
				sendto(sock, send_data, strlen(send_data->str), 0,(struct sockaddr *)&client_addr, sizeof(struct sockaddr));
				bytes_read = recvfrom(sock,recv_data,1024,0,(struct sockaddr *)&client_addr, &addr_len);
				recv_data->str[bytes_read] = '\0';
				if(strcmp(recv_data->str,"Ack")==0)
				{
					printf("Ack received\n");
					count=0;
				}
			}
			bytes_read = recvfrom(sock,recv_data,1024,0,(struct sockaddr *)&client_addr, &addr_len);
			recv_data->str[bytes_read] = '\0';
			if ( strcmp (recv_data->str,"ACK") == 0  )
			{
				printf("Recive ACK\n");
			}
			strcpy(send_data->str,"Done");
			sendto(sock, send_data, strlen(send_data->str), 0,(struct sockaddr *)&client_addr, sizeof(struct sockaddr));
		}
		else if(strcmp(r_data[0],"FileUpload")==0)
		{
			int fp;
			fp=open("Sravya", O_CREAT | O_RDWR | O_TRUNC, 0777);
			while(1)
			{
				char Buffer[3]="";
				strcpy(Buffer,"Hi");
				bytes_read = recvfrom(sock,Buffer,sizeof(Buffer),0,(struct sockaddr *)&client_addr, &addr_len);
				recv_data->str[bytes_read] = '\0';
				sendto(sock, send_data, strlen(send_data->str), 0,(struct sockaddr *)&client_addr, sizeof(struct sockaddr));
				if ( strcmp (Buffer,"Hi") == 0  )
				{
					printf("man\n");
					break;
				}
				else
				{
					printf("gm\n");
					write(fp,Buffer,2);
				}
			}
			close(fp);
			sendto(sock,"ACK",3, 0,(struct sockaddr *)&client_addr, sizeof(struct sockaddr));
			printf("ACK Send");
		}
		else if(strcmp(r_data[0],"FileDownload")==0)
		{
			int fd;
			fd=open(r_data[1],O_RDONLY);
			char Buffer[3] = "";
			int len;
			int fd1;
			while ((fd1= read(fd,Buffer,2)) > 0)
			{  
				int count=1;
				printf("sending\n");
				while(count!=0)
				{
			sendto(sock,Buffer,sizeof(Buffer), 0,(struct sockaddr *)&client_addr, sizeof(struct sockaddr));
			bytes_read = recvfrom(sock,recv_data,sizeof(packet),0,(struct sockaddr *)&client_addr, &addr_len);
			recv_data->str[bytes_read] = '\0';
				if(strcmp(recv_data->str,"Ack")==0)
				{
					count=0;
				}
				}
			}
			strcpy(Buffer,"Hi");
			sendto(sock,Buffer,sizeof(Buffer), 0,(struct sockaddr *)&client_addr, sizeof(struct sockaddr));
			char Buf[5];
			bytes_read = recvfrom(sock,Buf,sizeof(Buf),0,(struct sockaddr *)&client_addr, &addr_len);
			recv_data->str[bytes_read] = '\0';
			if ( strcmp (Buf,"ACK") == 0  )
			{
				printf("Recive ACK\n");
			}        
			close(fd);
			strcpy(send_data->str,"Done");
			sendto(sock,send_data,sizeof(packet), 0,(struct sockaddr *)&client_addr, sizeof(struct sockaddr));
		}

		fflush(stdout);
	}
	return 0;
}
