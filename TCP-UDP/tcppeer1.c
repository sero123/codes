/* tcpserver.c */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/sendfile.h>
#include <sys/stat.h>
#include<ctype.h>
#include<regex.h>
//struct for sending packets
struct packet
{
	//int ack;
	int last;
	//char IP[15];
	char str[1002];
};
typedef struct packet packet;
packet *send_data;
packet *recv_data;
#define STR_VALUE(val) #val
#define STR(name) STR_VALUE(name)
#define PATH_LEN 256
#define MD5_LEN 32

int flagq=0,cnt=0;
char string[40000];
int flagreg=0,short_list=0;
char r_data[20][1024],patt[100],date2[100],date3[100];
//function to change size of file to sring
char* call(long long i, char b[]){
	char const digit[] = "0123456789";
	char* p = b;
	if(i<0){
		*p++ = '-';
		i = -1;
	}
	int shifter = i;
	do{ //Move to where representation ends
		++p;
		shifter = shifter/10;
	}while(shifter);
	*p = '\0';
	do{ //Move back, inserting digits as u go
		*--p = digit[i%10];
		i = i/10;
	}while(i);
	return b;
}
// function to compare dates
time_t to_seconds(const char *date)
{
        struct tm storage={0,0,0,0,0,0,0,0,0};
        char *p=NULL;
        time_t retval=0;

        p=(char *)strptime(date,"%d-%b-%Y",&storage);
        if(p==NULL)
        {
                retval=0;
        }
        else
        {
                retval=mktime(&storage);
        }
        return retval;
}
//function for command FileHash CheckAll
void CheckAll()
{
	//printf("check\n");
	int Fp,con=1,m=0,n=0;
	char buf[2];
	char file[100];
	char md5[MD5_LEN + 1];
	struct stat sb;
	Fp=open("temp1",O_RDONLY);
	if (Fp == -1) 
	{
		fprintf(stderr, "unable to open temp1: %s\n", strerror(errno));
		exit(1);
	}
	while(1)
	{
		//printf("while\n");
		con=read(Fp,buf,1);
		if(con==0)
			break;
		if(buf[0]=='\n')
		{
			file[m]='\0';

			if (!CalcFileMD5(file, md5))
			{
				puts("Error occured!");
			}
			else
			{
			strcat(string,"\nFile Hash Value : ");
				strcat(string,md5);
			}
			strcat(string,"\n");
			strcat(string,"File Name :");
			strcat(string,file);
			strcat(string,"\n");
			strcat(string,"Last Modified Time :");
			stat(file,&sb);
			strcat(string, ctime(&sb.st_mtime));
			strcat(string,"File Size :");
        char b[1000];
          strcat(string, call(sb.st_size,b));
			m=0;
			n=n+1;
		}
		else
		{
			file[m]=buf[0];
			m++;
		}
		//printf("%d\n",con);
	}
}
//Function to find the hash value of a file
int CalcFileMD5(char *file_name, char *md5_sum)
{
#define MD5SUM_CMD_FMT "md5sum %." STR(PATH_LEN) "s 2>/dev/null"
	char cmd[PATH_LEN + sizeof (MD5SUM_CMD_FMT)];
	sprintf(cmd, MD5SUM_CMD_FMT, file_name);
#undef MD5SUM_CMD_FMT
	FILE *p = popen(cmd, "r");
	if (p == NULL) return 0;
	int i, ch;
	for (i = 0; i < MD5_LEN && isxdigit(ch = fgetc(p)); i++) {
		*md5_sum++ = ch;
	}
	*md5_sum = '\0';
	pclose(p);
	return i == MD5_LEN;
}	
//Function to match files with  regular expressions
int match(char text[],char pattern[])
{
      regex_t    preg;
   int        rc;
   size_t     nmatch = 2;
   regmatch_t pmatch[2];

if (0 != (rc = regcomp(&preg, pattern, 0))) {
         return -10;
   }

   if (0 != (rc = regexec(&preg, text, nmatch, pmatch, 0))) {
       return -10;
   }
   else {
  return 1;
   }
   regfree(&preg);

}
//function to parse time
char * change_time(char *new,char *old)
{
char delims[] = " ";
    char *parts = NULL;
    parts = strtok( old, delims );
    char r_date[20][2024];
    int d_cnt=0,j;
    while( parts != NULL )
    {
            strcpy(r_date[d_cnt],parts);
            parts = strtok( NULL, delims );
            d_cnt++;
    }
     strcpy(new,r_date[3]);
//printf("date1 is %s\n",date1);
return new;
}
//function to parse dates
char * change(char *new,char *old)
{
char delims[] = " ";
    char *parts = NULL;
    parts = strtok( old, delims );
    char r_date[20][2024];
    int d_cnt=0,j;
    while( parts != NULL )
    {
            strcpy(r_date[d_cnt],parts);
            parts = strtok( NULL, delims );
            d_cnt++;
    }
     strcpy(new,r_date[2]);
     strcat(new,"-");
     strcat(new,r_date[1]);
     strcat(new,"-");
     strcat(new,r_date[4]);
    // printf("date1 is %s\n",date1);
     return new;
}
//function to give file information using stat
void file_info(char *file)
{
	struct stat sb;
	stat(file, &sb);
	char  date1[1024];
	char  date2c[1024];
	char  date3c[1024];
	char  date1t[1024];
	char  date2t[1024];
	char  date3t[1024];
	char  date33[1024];
	char  date22[1024];
	 strcpy(date22,date2);
	 strcpy(date33,date3);
	strcpy(date1,change(date1,ctime(&sb.st_mtime)));
	strcpy(date2c,change(date2c,date22));
	strcpy(date3c,change(date3c,date33));
	strcpy(date1t,change_time(date1t,ctime(&sb.st_mtime)));
	strcpy(date2t,change_time(date2t,date22));
	strcpy(date3t,change_time(date3t,date33));
	time_t d1=to_seconds(date1);
	time_t d2=to_seconds(date2c);
	time_t d3=to_seconds(date3c);

//	printf("date comparison: %s %s ",date1,date2);
	//     if(d1==d2) printf("equal\n");
	//   if(d2>d1)  printf("second date is later\n");
	// if(d2<d1)  printf("seocnd date is earlier\n");
int 	flag=0;
	if(short_list==1)
	{
		if(d1 > d2 && d1 <d3 )
		{
			flag=1;
		}
		if(d2==d3)
		{
			if(strcmp(date1t,date2t)>=0 && strcmp(date1t,date3t)<=0)
				flag=1;
		}
                if(d1==d2)
                  {
			if(strcmp(date1t,date2t)>=0 )
				flag=1;}  
                if(d1==d3)
                  {
			if(strcmp(date1t,date3t)<=0 )
				flag=1;}  
		if(flag==1)
		{
	//printf("File Name:%s\n",file);

             strcat(string,"\nFile Name : ");
                strcat(string,file);
                        strcat(string,"\n");
        strcat(string,"File type:                ");
        switch (sb.st_mode & S_IFMT) {
                case S_IFBLK:  strcat(string,"block device\n");            break;
                case S_IFCHR:  strcat(string,"character device\n");        break;
                case S_IFDIR:  strcat(string,"directory\n");               break;
                case S_IFIFO:  strcat(string,"FIFO/pipe\n");               break;
                case S_IFLNK:  strcat(string,"symlink\n");                 break;
                case S_IFREG:  strcat(string,"regular file\n");            break;
                case S_IFSOCK: strcat(string,"socket\n");                  break;
                default:       strcat(string,"unknown?\n");                break;
        }
                //      strcat(string,"\n");
                strcat(string,"Last Modified time : ");
                        strcat(string,ctime(&sb.st_mtime));
        //              strcat(string,"\n");
                strcat(string,"file size in bytes : ");
        char b[1000];
          strcat(string, call(sb.st_size,b));
 cnt++;
        }

			//exit(EXIT_SUCCESS);
	}
	else
	{
	//printf("File Name:%s\n",file);
		strcat(string,"\nFile Name : ");
		strcat(string,file);
			strcat(string,"\n");
	strcat(string,"File type:                ");
	switch (sb.st_mode & S_IFMT) {
		case S_IFBLK:  strcat(string,"block device\n");            break;
		case S_IFCHR:  strcat(string,"character device\n");        break;
		case S_IFDIR:  strcat(string,"directory\n");               break;
		case S_IFIFO:  strcat(string,"FIFO/pipe\n");               break;
		case S_IFLNK:  strcat(string,"symlink\n");                 break;
		case S_IFREG:  strcat(string,"regular file\n");            break;
		case S_IFSOCK: strcat(string,"socket\n");                  break;
		default:       strcat(string,"unknown?\n");                break;
	}
		//	strcat(string,"\n");
		strcat(string,"Last Modified time : ");
			strcat(string,ctime(&sb.st_mtime));
	//		strcat(string,"\n");
		strcat(string,"file size in bytes : ");
        char b[1000];
          strcat(string, call(sb.st_size,b));
	}
}
//function to recurrese through directories and files
void listdir(const char *name, int level)
{
	DIR *dir;
	int s;
	struct dirent *entry;

	if (!(dir = opendir(name)))
		return;
	if (!(entry = readdir(dir)))
		return;

	do {
		if (entry->d_type == DT_DIR) {
			char path[1024];
			int len = snprintf(path, sizeof(path)-1, "%s/%s", name, entry->d_name);
			path[len] = 0;
			if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
				continue;
			if(flagreg==1)
			{
				if(match(entry->d_name,patt)!=-10 )
				{
					//strcat(string,entry->d_name);
					file_info(entry->d_name);
					strcat(string,"\n");
					listdir(path, level + 1);
				}
			}
			else
			{
				//strcat(string,entry->d_name);
				file_info(entry->d_name);
				strcat(string,"\n");
				listdir(path, level + 1);
			}
		}
		else
		{

			//   printf("%*s- %s\n", level*2, "", entry->d_name);
			if(flagreg==1)
			{
				if(match(entry->d_name,patt)!=-10)
				{
					//strcat(string,entry->d_name);
					file_info(entry->d_name);
					strcat(string,"\n");
				}
			}
			else
			{
				//strcat(string,entry->d_name);
				file_info(entry->d_name);
				strcat(string,"\n");
			}
		}
	} while (entry = readdir(dir));
	closedir(dir);
}

int main()
{
	//int y;
	//for(y=0;y<39999;y++)
	//	string[y]="";
	 string[0]='\0';
         send_data= malloc(sizeof(packet));
         recv_data= malloc(sizeof(packet));
	int sock, connected, bytes_recieved , true = 1;  

	struct sockaddr_in server_addr,client_addr;    
	int sin_size;
	
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("Socket");
		exit(1);
	}
	//char *store;
	/*system("/sbin/ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}' > temp");
	//system("ls $(find -type f) > temp");
	//strcat(store,"\0");
	//printf("%s\n",send_data->IP);
	int gp,gp1;
	gp=open("temp",O_RDONLY);
	gp1=read(gp,send_data->IP,15);
	close(gp);*/
	server_addr.sin_family = AF_INET;         
	server_addr.sin_port = htons(5000);     
	server_addr.sin_addr.s_addr = INADDR_ANY; 
	bzero(&(server_addr.sin_zero),8); 

	if (bind(sock, (struct sockaddr *)&server_addr, sizeof(struct sockaddr))
			== -1) {
		perror("Unable to bind");
		exit(1);
	}
	if (listen(sock, 5) == -1) {
		perror("Listen");
		exit(1);
	}
	printf("\nTCPServer Waiting for client on port 5000");
	fflush(stdout);
	while(1)
	{  
		sin_size = sizeof(struct sockaddr_in);
		connected = accept(sock, (struct sockaddr *)&client_addr,&sin_size);
		printf("\n I got a connection from (%s , %d)",
				inet_ntoa(client_addr.sin_addr),ntohs(client_addr.sin_port));
		while (1)
		{
			string[0]='\0';
			printf("\n SEND (q or Q to quit) : ");
				gets(send_data->str);
			if (strcmp(send_data->str , "q") == 0 || strcmp(send_data->str , "Q") == 0)
			{
				flagq=1;
				send(connected, send_data,sizeof(packet), 0); 
				close(connected);
				break;
			}
			else
			{
				if (strcmp(r_data[0] , "get_files") == 0 )
				{
					listdir(".",2);
					if(strlen(string)<=1000)
					{
						printf("hi\n");
						strcpy(send_data->str,string);
						send(connected, send_data,sizeof(packet), 0);  
					}
					else if(strlen(string)>1000)
					{
						int j=strlen(string)/1000;
						printf("j is %d\n",j);
						int k=0;
						int p;
						int l,i;
						while(j>=0)
						{
							l=0;
							for(i=k;i<k+1000;i++)
							{
								send_data->str[l]=string[i];
								l++;
							}
							send(connected, send_data,sizeof(packet), 0);  
							j--;
							k=k+1000;
						}
					}
					strcpy(send_data->str,"Hi");
					send(connected,send_data,sizeof(packet),0);
					recv(connected,recv_data, sizeof(packet), 0);
					if ( strcmp (recv_data->str,"ACK") == 0  )
					{
						printf("Recive ACK\n");
					}
					strcpy(send_data->str,"Done");
					send(connected, send_data,sizeof(packet), 0);  
				}
				else if (strcmp(r_data[0] , "Regex") == 0 )
				{
					strcpy(patt,r_data[1]);
					flagreg=1;
					listdir(".",2);
					flagreg=0;
					if(strlen(string)<=1000)
					{
						printf("hi\n");
						strcpy(send_data->str,string);
						send(connected, send_data,sizeof(packet), 0);  
					}
					else if(strlen(string)>1000)
					{
						int j=strlen(string)/1000;
						printf("j is %d\n",j);
						int k=0;
						int p;
						int l,i;
						while(j>=0)
						{
							l=0;
							for(i=k;i<k+1000;i++)
							{
								send_data->str[l]=string[i];
								l++;
							}
							send(connected, send_data,sizeof(packet), 0);  
							j--;
							k=k+1000;
						}
					}
					strcpy(send_data->str,"Hi");
					send(connected,send_data,sizeof(packet),0);
					recv(connected,recv_data, sizeof(packet), 0);
					if ( strcmp (recv_data->str,"ACK") == 0  )
					{
						printf("Recive ACK\n");
					}
					strcpy(send_data->str,"Done");
					send(connected, send_data,sizeof(packet), 0);  
				}
				else if (strcmp(r_data[0] , "short_list") == 0 )
				{
 
					strcpy(date2,r_data[1]);
					strcat(date2," ");

					strcat(date2,r_data[2]);
					strcat(date2," ");
					strcat(date2,r_data[3]);
					strcat(date2," ");
					strcat(date2,r_data[4]);
					strcat(date2," ");
					strcat(date2,r_data[5]);
					strcat(date2,"\0");
					strcpy(date3,r_data[6]);
					strcat(date3," ");
					strcat(date3,r_data[7]);
					strcat(date3," ");
					strcat(date3,r_data[8]);
					strcat(date3," ");
					strcat(date3,r_data[9]);
					strcat(date3," ");
					strcat(date3,r_data[10]);
					strcat(date3,"\0");
					short_list=1;
                                  cnt=0;
					listdir(".",2);
					short_list=0;
                               if(cnt==0)
                                      strcpy(string,"no such file\0"); 
					if(strlen(string)<=1000)
					{
						printf("hi\n");
						strcpy(send_data->str,string);
						send(connected, send_data,sizeof(packet), 0);  
					}
					else if(strlen(string)>1000)
					{
						int j=strlen(string)/1000;
						printf("j is %d\n",j);
						int k=0;
						int p;
						int l,i;
						while(j>=0)
						{
							l=0;
							for(i=k;i<k+1000;i++)
							{
								send_data->str[l]=string[i];
								l++;
							}
							send(connected, send_data,sizeof(packet), 0);  
							j--;
							k=k+1000;
						}
					}
					strcpy(send_data->str,"Hi");
					send(connected,send_data,sizeof(packet),0);
					recv(connected,recv_data, sizeof(packet), 0);
					if ( strcmp (recv_data->str,"ACK") == 0  )
					{
						printf("Recive ACK\n");
					}
					strcpy(send_data->str,"Done");
					send(connected, send_data,sizeof(packet), 0);  
				}
				else if (strcmp(r_data[0] , "FileDownload")==0)
				{
					int flagd=0;
					if(strcmp(send_data->str,"Deny")==0)
					{
						strcpy(send_data->str,"Deny");
						send(connected,send_data,sizeof(packet),0);
						flagd=1;
							//printf("DONT Down\n");
					}
					send(connected,send_data,sizeof(packet),0);
					int fd;
					fd=open(r_data[1],O_RDONLY);	
				      if (fd == -1) {
					      fprintf(stderr, "unable to open '%s': %s\n", r_data[1], strerror(errno));
					      exit(1);
					}
					char Buffer[3] = "";
					int len;
					int fd1;
					while ((fd1= read(fd,Buffer,2)) > 0 && flagd!=1)
					{   
						printf("sending file\n");
						send(connected,Buffer,sizeof(Buffer),0);            
					}
					printf("dunn\n");
					strcpy(Buffer,"Hi");
					send(connected,Buffer,sizeof(Buffer),0);
					char Buf[5];
					recv(connected, Buf, 5, 0);
					if ( strcmp (Buf,"ACK") == 0  )
					{
						printf("Recive ACK\n");
					}        
					close(fd);
					strcpy(send_data->str,"Done");
					send(connected, send_data,sizeof(packet), 0);  
				}
				else if(strcmp(r_data[0],"FileUpload")==0)
				{ 
						int flag=0;
				/*		char md5[MD5_LEN + 1];
						if (!CalcFileMD5(r_data[2], md5))
						{
							puts("Error occured!");
						}
						else
						{
							//printf("Success! MD5 sum is: %s\n", md5);
							strcpy(send_data->str,md5);
						}

						printf("hmm\n");
						int fp;
						strcat(send_data->str,r_data[1]);
						struct stat sb;
						strcat(send_data->str," ");
						stat(r_data[1],&sb);
						strcat(send_data->str, ctime(&sb.st_mtime));
						strcat(send_data->str," ");*/
						char b[1000];
						struct stat sb;
						stat(r_data[1],&sb);
						strcpy(send_data->str, call(sb.st_size,b));
						strcat(send_data->str,"\n");
						printf("string%s\n",send_data->str);
						send(connected, send_data,sizeof(packet), 0);
						if(atoi(send_data->str)>9000)
						{
							flag=1;
						} 
						int fp;
						if(flag!=1)
						fp=open(r_data[1], O_CREAT | O_RDWR | O_TRUNC, 0777);
				      if (fp == -1) {
					      fprintf(stderr, "unable to open '%s': %s\n", r_data[1], strerror(errno));
					      exit(1);
					}
						while(flag!=1)
						{
							char Buffer[3]="";
							strcpy(Buffer,"Hi");
							if (recv(connected, Buffer, sizeof(Buffer), 0))
							{
								if ( strcmp (Buffer,"Hi") == 0  )
								{
									//printf("man\n");
									break;
								}
								else
								{
									//printf("gm\n");
									write(fp,Buffer,2);
								}
							}
						}
						close(fp);
						send(sock, "ACK" ,3,0);
						printf("ACK Send");
					//}
					/*else if(strcmp(send_data->str,"FileUpload-Deny")==0)

					{ 

                                      //    recv(connected,recv_data, sizeof(packet), 0);
                                               
                                              
                                             
			//			strcpy(send_data->str,":(");
			//			send(connected, send_data,sizeof(packet), 0);  

					}*/
				}
				else if(strcmp(r_data[0],"FileHash")==0 && strcmp(r_data[1],"Verify")==0)
				{
					char Buff[100];
					char md5[MD5_LEN + 1];

					if (!CalcFileMD5(r_data[2], md5)) 
					{
						puts("Error occured!");
					} 
					else 
					{
						//printf("Success! MD5 sum is: %s\n", md5);
					strcpy(send_data->str,"\nFile Hash Value :");
						strcat(send_data->str,md5);
					}
					strcat(send_data->str,"\n");
					strcat(send_data->str,"File Name :");
					strcat(send_data->str,r_data[2]);
					strcat(send_data->str,"\n");
					struct stat sb;
					strcat(send_data->str,"Last Modified time :");
					stat(r_data[2],&sb);
					strcat(send_data->str, ctime(&sb.st_mtime));
			//		strcat(send_data->str,"\n");
					strcat(send_data->str,"File Size :");
        char b[1000];
          strcat(send_data->str, call(sb.st_size,b));
					send(connected, send_data,sizeof(packet), 0);  


                                         strcpy(send_data->str,"Hi");
                                        send(connected,send_data,sizeof(packet),0);
                                        recv(connected,recv_data, sizeof(packet), 0);
                                        if ( strcmp (recv_data->str,"ACK") == 0  )
                                        {
                                                printf("Recive ACK\n");
                                        }
                                        strcpy(send_data->str,"Done");
                                        send(connected, send_data,sizeof(packet), 0);

				}
				else if(strcmp(r_data[0],"FileHash")==0 && strcmp(r_data[1],"CheckAll")==0)
				{
					system("ls $(find -type f) > temp1");
					CheckAll();
					printf("jmm\n");
					//send(connected, send_data,sizeof(packet), 0);
					if(strlen(string)<=1000)
					{
						printf("hi\n");
						strcpy(send_data->str,string);
						send(connected, send_data,sizeof(packet), 0);  
					}
					else if(strlen(string)>1000)
					{
						int j=strlen(string)/1000;
						printf("j is %d\n",j);
						int k=0;
						int p;
						int l,i;
						while(j>=0)
						{
							l=0;
							for(i=k;i<k+1000;i++)
							{
								send_data->str[l]=string[i];
								l++;
							}
							send(connected, send_data,sizeof(packet), 0);  
							j--;
							k=k+1000;
						}
					}
					strcpy(send_data->str,"Hi");
					send(connected,send_data,sizeof(packet),0);
					recv(connected,recv_data, sizeof(packet), 0);
					if ( strcmp (recv_data->str,"ACK") == 0  )
					{
						printf("Recive ACK\n");
					}
					strcpy(send_data->str,"Done");
					send(connected, send_data,sizeof(packet), 0);    
				}
				else
				{
					send(connected, send_data,sizeof(packet), 0);  
				}
			}
			bytes_recieved = recv(connected,recv_data,sizeof(packet),0);
			char delims[] = " ";
			char *parts = NULL;
			parts = strtok( recv_data->str, delims );
			int recv_cnt=0,j;
			while( parts != NULL ) 
			{
				strcpy(r_data[recv_cnt],parts);
				parts = strtok( NULL, delims );
				recv_cnt++;
				if(recv_cnt==15)
					break;
			}
				      char de[] = " ";
				      char s_data[20][2024];
				      int s_cnt=0;
			if (strcmp(recv_data->str , "q") == 0 || strcmp(recv_data->str , "Q") == 0)
			{
				flagq=1;
				close(connected);
				break;
			}
			else
			{
				printf("\n RECIEVED DATA = %s " , recv_data->str);
                              int pp=memcmp(send_data->str,"get_files",9);
                              int pp1=memcmp(send_data->str,"short_list",10);
                              int pp2=memcmp(send_data->str,"Regex",5);
			      int pp3=memcmp(send_data->str,"FileHash Verify",15);
			      int pp4=memcmp(send_data->str,"FileHash CheckAll",16);
				      char *pa = NULL;
				      pa = strtok( send_data->str, de );
				      while( pa != NULL )
				      {
					      strcpy(s_data[s_cnt],pa);
					      pa= strtok( NULL, de);
					      s_cnt++;
                  if(s_cnt==15)
            break;

				      }
			      if(strcmp(s_data[0],"FileDownload")==0)
			      {
				      int fp;
				      fp=open(s_data[1], O_CREAT | O_RDWR | O_TRUNC, 0777);
				      if (fp == -1) {
					      fprintf(stderr, "unable to open '%s': %s\n", s_data[1], strerror(errno));
					      exit(1);
					}
				      while(1)
				      {
					      char Buffer[3]="";
					      if (recv(connected, Buffer, sizeof(Buffer), 0))
					      {
						      if ( strcmp (Buffer,"Hi") == 0  )
						      {
							      break;
						      }
						      else
						      {
							      write(fp,Buffer,2);
                                                               Buffer[0]='\0';
						      }
					      }
				      }
				      close(fp);
				      send(connected, "ACK" ,3,0);
				      printf("ACK Send");
			      }
			      else if(strcmp(s_data[0],"FileUpload")==0)
			      {
				      int fd;
				      fd=open(s_data[1],O_RDONLY);
				      if (fd == -1) {
					      fprintf(stderr, "unable to open '%s': %s\n", s_data[1], strerror(errno));
					      exit(1);
				      }
				      char Buffer[3] = "";
				      int len;
				      int fd1;
				      while ((fd1= read(fd,Buffer,2)) > 0)
				      {   
					      printf("sending file\n");
					      send(connected,Buffer,sizeof(Buffer),0);            
				      }
				      printf("lol\n");
				      strcpy(Buffer,"Hi");
				      send(connected,Buffer,sizeof(Buffer),0);
				      char Buf[5];
				      recv(connected, Buf, 5, 0);
				      if ( strcmp (Buf,"ACK") == 0  )
				      {
					      printf("Recive ACK\n");
				      }        
				      //close (sock);
				      close(fd);
				      strcpy(send_data->str,"Done");
				      send(connected, send_data,sizeof(packet), 0);  
			      }
			      else if(pp==0 || pp1==0 || pp2==0 || pp3==0 || pp4==0)
				{ 


					while(1)
					{
						if (recv(connected, recv_data, sizeof(packet), 0))
						{
							if ( strcmp (recv_data->str,"Hi") == 0  )
							{
								printf("dif\n");
								break;
							}
							else
							{
								//write(fp,Buffer,2);
								printf("%s",recv_data->str);
							}
						}
					}
					strcpy(recv_data->str,"ACK");
					//strcat(recv_data->str,'\0');
					send(connected, recv_data ,sizeof(packet),0);
					printf("ACK Send");
					recv(connected,recv_data,sizeof(packet),0);
	   				printf("\nRECIEVED DATA %s\n",recv_data->str);
				}
			}
			fflush(stdout);
		}
		if(flagq==1)
		{
			break;
		}
	}       
	close(sock);
	return 0;
} 
