#!/usr/bin/env python
import os
import sys
from math import fabs
from math import sqrt
from math import log
import numpy as np
	


for m in range(1,11):
	name="AD/ad_train"+str(m)
	f1=open(name,"r")
	name="AD/ad_test"+str(m)
	f2=open(name,"r")
	name="AD/ad_res"+str(m)
	f3=open(name,"w")

	lines=f1.readlines()
	nattr=len(lines[0].split(','))-1
	prob1={}
	prob2={}
	for i in range(0, nattr):
		prob1[i]={}
		prob2[i]={}
	count1=0
	count2=0
	for line in lines:
		line=line.split(',')
		if line[-1]==">50K\n":
			count1+=1
			for i in range(0,nattr):
				if line[i] in prob1[i].keys():
					prob1[i][line[i]]+=1
				else:
				 	prob1[i][line[i]]=1
		else:
		   	count2+=1
			for i in range(0,nattr):
				if line[i] in prob2[i].keys():
					prob2[i][line[i]]+=1
				else:
				 	prob2[i][line[i]]=1
	
	for line in f2:
		line=line.split(',')
		p1=log(float(count1)/(count1+count2))
		p2=log(float(count2)/(count1+count2))
		for i in range(0, nattr):
			if line[i] in prob1[i].keys():
				p1+=log(prob1[i][line[i]]/(1.0*count1))
			if line[i] in prob2[i].keys():
				p2+=log(prob2[i][line[i]]/(1.0*count2))
		f3.write(line[-1][:-1])
		f3.write(" ")
		if p1>p2:
		  f3.write(">50K\n")
		else:
		  f3.write("<=50K\n")


