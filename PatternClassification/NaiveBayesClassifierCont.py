#!/usr/bin/env python
import os
import sys
from math import fabs
from math import sqrt
from math import log
import numpy as np
	
def calc(val, m, v):
	sq=(val-m)*(val-m)
	sq=float(sq)/(2*v)
	vl=1.0/(sqrt(2*3.14*v))
	return log(vl)-sq

for m in range(1,2):
	name="CA/can_train"+str(m)
	f1=open(name,"r")
	name="CA/can_test"+str(m)
	f2=open(name,"r")
	name="CA/can_res"+str(m)
	f3=open(name,"w")

	lines=f1.readlines()
	lv1={}
	lv2={}
	meann1={}
	meann2={}
	varr2={}
	varr1={}
	p_class={"2":0,"4":0}
	nattr=len(lines[0].split(','))-1
	for i in range(0,nattr):
		lv1[i]=[]
		lv2[i]=[]
		meann1[i]=0
		meann2[i]=0
		varr1[i]=0
		varr2[i]=0

	for i in range(0,len(lines)):
		lines[i]=lines[i].split(',')
		if lines[i][-1]=="2\n":
			for j in range(0, nattr):
				lv1[j].append(float(lines[i][j]))
			p_class["2"]+=1
		else:
			for j in range(0, nattr):
				lv2[j].append(float(lines[i][j]))
			p_class["4"]+=1
	
	for i in range(0,nattr):
		meann1[i]=float(np.mean(lv1[i]))
		meann2[i]=float(np.mean(lv2[i]))
		varr1[i]=float(np.var(lv1[i]))
		varr2[i]=float(np.var(lv2[i]))

	p_class["2"]=p_class["2"]*1.0/len(lines)
	p_class["4"]=p_class["4"]*1.0/len(lines)
	
	for line in f2:
		line=line.split(',')
		p1=log(p_class["2"])
		for i in range(0,nattr):
			p1+=calc(float(line[i]),meann1[i],varr1[i])
		p2=log(p_class["4"])
		for i in range(0,nattr):
			p2+=calc(float(line[i]),meann2[i],varr2[i])
		print p1,
		print p2
		f3.write(line[-1][:-1])
		f3.write(" ")
		if p1>p2:
			f3.write("2")
		else:
			f3.write("4")
		f3.write("\n")

	
			

	

