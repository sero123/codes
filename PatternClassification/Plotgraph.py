import matplotlib.pyplot as plt
import matplotlib.lines as lines 
from operator import itemgetter
data = open("f.txt").read().split("\n")
data.pop()
plt.axes()
x = []
y = []
L = []
colors={"Iris-versicolor":"red","Iris-setosa":"green","Iris-virginica":"blue"}
col=[]

for i in range(len(data)):
	sp=data[i].split()
	
	if sp==[]:
		continue
	sp[0]=float(sp[0])*10
	sp[1]=float(sp[1])*10
	col.append(colors[sp[2]])
	L.append(sp)
L=sorted(L, key=itemgetter(0,1))

st1=20
st2=1
dw=0.5
ll=[]
prev=[]
while(st2<26):
	prev=[]
	st1=20
	while(st1<45):
		for point in L:
			if int(point[0])==st1 and int(point[1])==st2:
				if prev==[]:
					prev=point
				elif point[2]!=prev[2] and prev[0]!= point[0]:
					print "vline"
					print point[0], point[1], point[2]
					print prev[0], prev[1], prev[2]
					pp=[]
					pp.append(((st1+prev[0])*1.0/2,dw))
					pp.append(((st1+prev[0])*1.0/2,dw+1))
					ll.append(pp)
					print pp
					prev=point
				elif point[2]==prev[2]:
					prev=point
		st1+=1
	dw+=1
	st2+=1

st1=20
dw=19.5
while(st1<45):
	prev=[]
	st2=1
	while(st2<26):
		for point in L:
			if point[1]==st2 and point[0]==st1:
				if prev==[]:
					prev=point
				elif point[2]!=prev[2] and prev[1] != point[1]:
					pp=[]
					print "hline"
					print point[0], point[1], point[2]
					print prev[0], prev[1], prev[2]

					pp.append((dw,(st2+prev[1])*1.0/2))
					pp.append((dw+1,(st2+prev[1])*1.0/2))
					ll.append(pp)
					prev=point
				elif point[2] == prev[2]:
					prev=point
		st2+=1
	dw+=1
	st1+=1

for line1 in ll:
	(line1_xs, line1_ys) = zip(*line1)
		
	lll=plt.Line2D(line1_xs, line1_ys, linewidth=2, color='black')
	plt.gca().add_line(lll)

for i in range(len(data)):
 sp = data[i].split()
 if sp==[]:
	continue
 x.append(float(sp[0])*10)
 y.append(float(sp[1])*10)
#print x
#print y
plt.scatter(x, y, c=col)
plt.show()
