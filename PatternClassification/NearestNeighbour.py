#!/usr/bin/env python
import os
import sys
from math import fabs
from math import sqrt
from random import randint

def sqrhamdist(string1, string2):
	dif=0
	length=min(len(string1),len(string2))
	for i in range(0,length):
		if string1[i]!=string2[i]:
			dif+=1
	dif+=abs(len(string1)-len(string2))
	return dif*dif

def tie(clas):
	maj=-1
	clasname=""
	for i in clas.keys():
		if clas[i] > maj:
			clasname=i
		elif clas[i] == maj:
			if randint(0,1)==0:
				clasname=i
	return clasname	
for m in range(1,11):
	name="iris/ir_train"+str(m)
	f1=open(name,"r")
	name="iris/ir_test"+str(m)
	f2=open(name,"r")
	name="iris/ir1_res"+str(m)
	f3=open(name,"w")

	lines=f1.readlines()
	for i in range(0,len(lines)):
		lines[i]=lines[i].split(',')
	nattr=len(lines[0])-1
	dist=0.0

	for line in f2:
		clas={}
		line=line.split(',')
		minim=999999
		for line1 in lines:
			for i in range(0,nattr):
				p=line[i][0]
				if p.isalpha():
					dist+=sqrhamdist(line[i],line1[i])
				else:
					dist+=fabs(float(line[i])-float(line1[i]))*fabs(float(line[i])-float(line1[i]))
			dist=sqrt(dist)
			if dist<minim:
				clas={}
				clas[line1[-1]]=1
				minim=dist
			elif dist==minim:
				if line1[-1] not in clas.keys():
					clas[line1[-1]]=1
				else:
		 			clas[line1[-1]]+=1
		f3.write(line[-1][:-1]+",")
		if len(clas)!=1:
	  		f3.write(tie(clas))

		else:
			l=clas.keys()
			f3.write(l[0])


