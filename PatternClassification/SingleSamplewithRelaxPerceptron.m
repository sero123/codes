w1 = [1 6; 7 2; 8 9; 9 9; 4 8; 8 5];
w2 = [2 1; 3 3; 2 4; 7 1; 1 3; 5 2];
%b = input('Enter the margin : ');
b = 0;
a0 = rand(1);
a1 = rand(1);    
a2 = rand(1);
% a0=0;
% a1=0;  0.240674
% a2=0;
% 
% a0=1;
% a1=1;
% a2=1;
% 
% a0=1;
% a1=2;
% a2=3;
% 
% a0=3;
% a1=2;
% a2=1;

d = size(w1,1);
w3 = [ones(size(w1,1), 1) w1];
w4 = [-ones(size(w2,1), 1) -w2];
a = [a0 a1 a2];

w = [w3;w4];
check = length(w);
flag = 1;
while(flag <= length(w))
    check = length(w);
    flag = 1;
    for j = 1:length(w)
        if(w(j,:)*a' <= b)
            dotpro = dot(a,a);
            p = ((b-(w(j,:)*a'))/dotpro);
            a = a + w(j,:)*((b-(w(j,:)*a'))/dotpro);
        else
            flag = flag + 1;
        end
    end
 end
 count
 
 x = -10:0.001:10;
 y = -10:0.001:10;

 y = -(a(2)*x + a(1))/a(3);

 plot(w1(:,1),w1(:,2),'og');
 title('d = 0')
 axis([-10,10 -10,10]);
 hold on;
 plot(w2(:,1),w2(:,2),'+r');
 axis([-10,10 -10,10]);
 hold on;
 plot(x,y);
 hold off;
