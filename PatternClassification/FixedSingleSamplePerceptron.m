tic
w1 = [1 6; 7 2; 8 9;9 9; 4 8; 8 5];
w2 = [2 1; 3 3; 2 4; 7 1; 1 3; 5 2];

d = size(w1,1);
w3 = [ones(size(w1,1), 1) w1];
w4 = [-ones(size(w2,1), 1) -w2];
a0=0;
a1=0;  
a2=0;



a = [a0 a1 a2];
a
%w2 = -w2;
w = [w3;w4];
length(w)

flag = 1;
while(flag ~= 0)
    flag = 0;
    for j = 1:length(w)
        y=w(j,:);
        if(a*y' < 0)
            a = a + w(j,:);
            flag = 1;
            break;
        end
    end
 end
 a
 x = 0:0.001:10;
 y = 0:0.001:10;

 y = -(a(2)*x + a(1))/a(3);

 plot(w1(:,1),w1(:,2),'og');
 axis([0,10 0,10]);
 hold on;
 plot(w2(:,1),w2(:,2),'+r');
 axis([0,10 0,10]);
 hold on;
 plot(x,y);
 hold off;
 toc
